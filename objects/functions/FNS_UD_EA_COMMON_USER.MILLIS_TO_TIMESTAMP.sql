create function FNS_UD_EA_COMMON_USER.MILLIS_TO_TIMESTAMP(P_MILLIS in NUMBER) return TIMESTAMP WITH TIME ZONE as
  V_DATEPART DATE;
  V_MILLISPART NUMBER;
  begin
    V_DATEPART := to_date('19700101', 'YYYYMMDD') + (trunc(P_MILLIS / 1000) / 86400);
    V_MILLISPART := mod(P_MILLIS, 1000);
    return from_tz(to_timestamp(to_char(V_DATEPART, 'YYYYMMDDHH24MISS') || substr(to_char(V_MILLISPART + 1000), 2, 3), 'YYYYMMDDHH24MISSFF'), 'Europe/Moscow');
  end;
/