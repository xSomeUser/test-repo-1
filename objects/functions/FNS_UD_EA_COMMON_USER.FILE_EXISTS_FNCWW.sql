create function FNS_UD_EA_COMMON_USER.FILE_EXISTS_FNCWW(FILE_DIR in VARCHAR2, FILE_NAME in VARCHAR2) return NUMBER as
  FIL BFILE;
  POS INTEGER;
  AMT BINARY_INTEGER;
  BUF RAW(400);
  begin
    select FSE.FILENAME into FIL from FTEWEB.FILE_SPACE_ENTRY FSE;
    DBMS_LOB.open(FIL, DBMS_LOB.LOB_READONLY);
    AMT := 40;
    POS := 1 + DBMS_LOB.getlength(FIL);
    BUF := '';
    DBMS_LOB.read(FIL, AMT, POS, BUF);
    DBMS_OUTPUT.put_line('Read F1 past EOF: ');
    DBMS_LOB.close(FIL);
    exception
    when no_data_found
    then
    begin
      DBMS_OUTPUT.put_line('End of File reached. Closing file');
      DBMS_LOB.fileclose(FIL);
      -- or dbms_lob.filecloseall if appropriate
    end;
  end;
/