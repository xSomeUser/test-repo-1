create function FNS_UD_EA_COMMON_USER.FILE_EXISTS_FNC(FILE_DIR in VARCHAR2, FILE_NAME in VARCHAR2) return NUMBER as
  V_EXISTS BOOLEAN;
  V_LENGTH NUMBER;
  V_BLOCKSIZE NUMBER;
  begin
    UTL_FILE.fgetattr(FILE_DIR, FILE_NAME, V_EXISTS, V_LENGTH, V_BLOCKSIZE);
    if V_EXISTS
    then
      return 1;
    else
      return 0;
    end if;
  end;
/