create function FNS_UD_EA_COMMON_USER.GET_REGDELO(ID1 in VARCHAR2) return sys_refcursor as
  ID_REGDELO VARCHAR2(32);
  ID2 VARCHAR2(32);
  ID3 VARCHAR2(32);
  DOCKIND VARCHAR2(10 BYTE);
  OUTPUTDATA sys_refcursor;
  begin
    select
      DOC.PARENTRECORDNUMBER, DOCKIND into ID2, DOCKIND
    from FNS_UD_EA_COMMON_USER.EA_CONVERT_DOC_CSR DOC
    where DOC.RECORDNUMBER = ID1 and DOC.CONVERTSTATUS in (0, 3, 6);
    if DOCKIND = 'EA_REGDELO' then
      ID_REGDELO := ID1;
    else
      if DOCKIND = 'EA_RDSET' then
        ID_REGDELO := ID2;
      else
        select
          DOC.PARENTRECORDNUMBER, DOCKIND into ID_REGDELO, DOCKIND
        from FNS_UD_EA_COMMON_USER.EA_CONVERT_DOC_CSR DOC
        where DOC.RECORDNUMBER = ID2;
      end if;
    end if;
    open OUTPUTDATA for
    select DOC_RD.* from FNS_UD_EA_COMMON_USER.EA_CONVERT_DOC_CSR DOC_RD
    where DOC_RD.RECORDNUMBER = ID_REGDELO
    union all select DOC_RDSET.* from FNS_UD_EA_COMMON_USER.EA_CONVERT_DOC_CSR DOC_RDSET
    where DOC_RDSET.PARENTRECORDNUMBER = ID_REGDELO
    union all select DOC_RD.* from FNS_UD_EA_COMMON_USER.EA_CONVERT_DOC_CSR DOC_RD
    join FNS_UD_EA_COMMON_USER.EA_CONVERT_DOC_CSR DOC_RDSET on (DOC_RD.PARENTRECORDNUMBER = DOC_RDSET.RECORDNUMBER and
        DOC_RDSET.PARENTRECORDNUMBER = ID_REGDELO);
    return OUTPUTDATA;
    exception
    when no_data_found then
    return null;
  end GET_REGDELO;
/