create function FNS_UD_EA_COMMON_USER.DETECT_PROCESS_NAME(RECNUM in VARCHAR2) return VARCHAR2 as
  PNAME VARCHAR2(255);
  ORIGINALPNAME VARCHAR2(255);
  begin
    select "ORIGPNAME" into ORIGINALPNAME from (
      select DR."F_CLASS" as "ORIGPNAME"
      from FNS_SYS_USER.VWVR1_DEFAULTROSTER DR
      where DR."F_CLASS" in ('fns-ea-lc-scan', 'fns-ea-lc-issuanceOfTemporaryUse', 'fns-ea-lc-scanAndDigitize', 'fns-ea-lc-selectiveScan', 'fns-ea-lc-store', 'fns-ea-lc-simpleTask', 'fns-ea-lc-digitize') and DR."RECORDNUMBER" = RECNUM
      order by DR."F_STARTTIME" desc
    )
    where ROWNUM <= 1;
    if (ORIGINALPNAME = 'fns-ea-lc-issuanceOfTemporaryUse') then PNAME := '������� �� ������';
    elsif (ORIGINALPNAME = 'fns-ea-lc-scan') then PNAME := '������� �� ������������';
    elsif (ORIGINALPNAME = 'fns-ea-lc-scanAndDigitize') then PNAME := '������� �� ������������ � ����';
    elsif (ORIGINALPNAME = 'fns-ea-lc-selectiveScan') then PNAME := '������� �� ���������� ������������';
    elsif (ORIGINALPNAME = 'fns-ea-lc-store') then PNAME := '������� ����������� �� ���������';
    elsif (ORIGINALPNAME = 'fns-ea-lc-simpleTask') then PNAME := '������� �� ������������ � ��';
    elsif (ORIGINALPNAME = 'fns-ea-lc-digitize') then PNAME := '������� �� ����';
    end if;
    return PNAME;
  end;
/