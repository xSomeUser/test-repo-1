create function FNS_UD_EA_COMMON_USER.UPDATE_OUTGOING_BUFFER_STATUS(OB_ID in NUMBER, OB_STATUS in VARCHAR2) return VARCHAR2 as
pragma autonomous_transaction;
  begin
    update FNS_UD_EA_COMMON_USER.OUTGOING_BUFFER
    set STATUS = OB_STATUS
    where ID = OB_ID;
    commit;
    return OB_STATUS;
  end UPDATE_OUTGOING_BUFFER_STATUS;
/