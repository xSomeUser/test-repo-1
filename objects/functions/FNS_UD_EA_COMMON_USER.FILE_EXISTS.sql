create function FNS_UD_EA_COMMON_USER.FILE_EXISTS(P_FNAME in VARCHAR2) return NUMBER as
  L_FILE UTL_FILE.FILE_TYPE;
  begin
    L_FILE := UTL_FILE.FOPEN(SUBSTR(P_FNAME, 1, instr(P_FNAME, '/', -1)), SUBSTR(P_FNAME, instr(P_FNAME, '/', -1) + 1), 'r');
    UTL_FILE.FCLOSE(L_FILE);
    return 1;
    exception
    when UTL_FILE.INVALID_PATH then return 4;
    when UTL_FILE.INVALID_OPERATION then return 3;
  end;
/