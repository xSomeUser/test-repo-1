create function FNS_UD_EA_COMMON_USER.GETNEXTREGISTRATIONCOUNTER(CNO in VARCHAR2, YY in VARCHAR2) return NUMBER as
  REG_COUNTER VARCHAR2(100);
  COUNTER_EXISTS NUMBER;
  SEQ_NAME VARCHAR2(100);
pragma autonomous_transaction;
  begin
    execute immediate ('SELECT ''CNO_' || YY || '_' || CNO || ''' FROM DUAL') into SEQ_NAME;
    select COUNT(*) into COUNTER_EXISTS from USER_SEQUENCES where SEQUENCE_NAME = SEQ_NAME;
    if COUNTER_EXISTS = 0 then
      execute immediate ('CREATE SEQUENCE ' || SEQ_NAME || ' START WITH 1 INCREMENT BY 1 NOMINVALUE NOMAXVALUE NOCYCLE NOCACHE');
    end if;
    execute immediate ('SELECT ' || SEQ_NAME || '.NEXTVAL FROM DUAL') into REG_COUNTER;
    return REG_COUNTER;
  end;
/