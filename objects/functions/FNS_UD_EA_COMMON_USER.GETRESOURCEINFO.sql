create function FNS_UD_EA_COMMON_USER.GETRESOURCEINFO("BUFFER_ID" in NUMBER, "I_BUFFERED_ZONE_EVENT_TYPE" VARCHAR2) return CLOB as
  BUFFER_RESOURCES_INFO CLOB;
  BR_RECORD_NUMBER VARCHAR2(32);
  BR_HASH_CODE VARCHAR2(255);
  BR_FILE_PATH VARCHAR2(255);
  BR_ID NUMBER(38, 0);
  BR_UUID VARCHAR2(32);
  BR_FILE_NAME VARCHAR2(255);
  BR_PUBLISHED_DATE TIMESTAMP(6);
  BR_STATUS VARCHAR2(255);
  cursor BUF_RESOURCES_INCOMING is
    select
      "ID",
      FILE_NAME,
      FILE_PATH,
      HASH_CODE,
      PUBLISHED_DATE,
      RECORD_NUMBER,
      STATUS,
      UUID
    from BUFFER_RESOURCE
    where ID in (select BUFFERED_RESOURCE_ID as "ID"
    from IB_RESOURCE
    where INCOMING_BUFFER_ID = BUFFER_ID);
  cursor BUF_RESOURCES_OUTGOING is
    select
      "ID",
      FILE_NAME,
      FILE_PATH,
      HASH_CODE,
      PUBLISHED_DATE,
      RECORD_NUMBER,
      STATUS,
      UUID
    from BUFFER_RESOURCE
    where ID in (select BUFFERED_RESOURCE_ID as "ID"
    from OB_RESOURCE
    where OUTGOING_BUFFER_ID = BUFFER_ID);
  begin
    BUFFER_RESOURCES_INFO := '{"COLUMNS": ["ID","FILE_NAME","FILE_PATH","HASH_CODE","PUBLISHED_DATE","RECORD_NUMBER","BR_STATUS","UUID"],"DATAROWS": [ ';
    ----------------------------------------------------------------------------
    -- SELECT BUFFER RESOURCES ACCORDING BUFFER TYPE INCOMING OR OUTGOING
    ----------------------------------------------------------------------------
    if I_BUFFERED_ZONE_EVENT_TYPE = 'INCOMING'
    then
      open BUF_RESOURCES_INCOMING;
      loop
        fetch BUF_RESOURCES_INCOMING
        into
        BR_ID,
        BR_FILE_NAME,
        BR_FILE_PATH,
        BR_HASH_CODE,
        BR_PUBLISHED_DATE,
        BR_RECORD_NUMBER,
        BR_STATUS,
        BR_UUID;
        exit when BUF_RESOURCES_INCOMING%notfound;
        BUFFER_RESOURCES_INFO := BUFFER_RESOURCES_INFO || '["' || BR_ID || '","' || BR_FILE_NAME || '","' || BR_FILE_PATH || '","' || BR_HASH_CODE || '","' || BR_PUBLISHED_DATE || '","' || BR_RECORD_NUMBER || '","' || BR_STATUS || '","' || BR_UUID || '"],';
      end loop;
      close BUF_RESOURCES_INCOMING;
    end if;
    ----------------------------------------------------------------------------
    if I_BUFFERED_ZONE_EVENT_TYPE = 'OUTGOING'
    then
      open BUF_RESOURCES_OUTGOING;
      loop
        fetch BUF_RESOURCES_OUTGOING
        into
        BR_ID,
        BR_FILE_NAME,
        BR_FILE_PATH,
        BR_HASH_CODE,
        BR_PUBLISHED_DATE,
        BR_RECORD_NUMBER,
        BR_STATUS,
        BR_UUID;
        exit when BUF_RESOURCES_OUTGOING%notfound;
        BUFFER_RESOURCES_INFO := BUFFER_RESOURCES_INFO || '["' || BR_ID || '","' || BR_FILE_NAME || '","' || BR_FILE_PATH || '","' || BR_HASH_CODE || '","' || BR_PUBLISHED_DATE || '","' || BR_RECORD_NUMBER || '","' || BR_STATUS || '","' || BR_UUID || '"],';
      end loop;
      close BUF_RESOURCES_OUTGOING;
    end if;
    ----------------------------------------------------------------------------
    -- END SELECT BUFFER RESOURCES ACCORDING BUFFER TYPE INCOMING OR OUTGOING
    ----------------------------------------------------------------------------
    -----------------------------------------------------------------------------
    -- REMOVE LAST COMMA
    if LENGTH(BUFFER_RESOURCES_INFO) > 0
    then
      BUFFER_RESOURCES_INFO := SUBSTR(BUFFER_RESOURCES_INFO, 0, LENGTH(BUFFER_RESOURCES_INFO) - 1);
    end if;
    BUFFER_RESOURCES_INFO := BUFFER_RESOURCES_INFO || ']}';
    return BUFFER_RESOURCES_INFO;
    return null;
  end GETRESOURCEINFO;
/