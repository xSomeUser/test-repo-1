create function FNS_UD_EA_COMMON_USER.SECONDS_TO_TIMESTAMP(P_MILLIS in NUMBER) return TIMESTAMP as
  begin
    return to_timestamp('1970-01-01 00', 'yyyy-mm-dd hh24') + (P_MILLIS) / 60 / 60 / 24;
  end;
/