create function FNS_UD_EA_COMMON_USER.F$DECODE_IR_STATUS(STATUS VARCHAR2, PART VARCHAR2) return NUMBER as
  RET_VAL NUMBER(1, 0) := 0;
  begin
    case UPPER(PART)
      when 'IN_PLAN' then
      case UPPER(STATUS)
        when 'IN_SENDING_QUEUE' then RET_VAL := 1;
        when 'IN_PRINT_SCHEDULE' then RET_VAL := 1;
      else RET_VAL := 0;
      end case;
      when 'PRINTING' then
      case UPPER(STATUS)
        when 'SENT_TO_PRINT' then RET_VAL := 1;
        when 'ACCEPTED' then RET_VAL := 1;
      else RET_VAL := 0;
      end case;
      when 'CANCELLED' then
      case UPPER(STATUS)
        when 'SENDING_CANCELED' then RET_VAL := 1;
        when 'NOT_ACCEPTED' then RET_VAL := 1;
        when 'ERROR' then RET_VAL := 1;
      else RET_VAL := 0;
      end case;
      when 'CREATED' then
      case UPPER(STATUS)
        when 'CREATED' then RET_VAL := 1;
      else RET_VAL := 0;
      end case;
    else RET_VAL := 0;
    end case;
    return (RET_VAL);
  end;
/