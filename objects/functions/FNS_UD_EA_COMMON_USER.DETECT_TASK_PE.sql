create function FNS_UD_EA_COMMON_USER.DETECT_TASK_PE(OBJID in VARCHAR2) return VARCHAR2 as
  TASKPE VARCHAR2(255);
  STATUS VARCHAR2(255);
  STORAGEPT VARCHAR2(255);
  NORSCANNORINPUTNORSTORECNT NUMBER(38, 0);
  CONTROLLAUNCHEDCNT NUMBER(38, 0);
  begin
    select
      DV.U05F8_STATUS, DV.UAC66_STORAGEPLACETYPE, R1.CNT, R3.CNT
    into STATUS, STORAGEPT, NORSCANNORINPUTNORSTORECNT, CONTROLLAUNCHEDCNT
    from (select * from (
      select
        ANN.U05F8_STATUS, DV.UAC66_STORAGEPLACETYPE, DV.U16D8_RECORDNUMBER
      from FNS_EA_CE_USER.DOCVERSION DV
      left join FNS_EA_CE_USER.ANNOTATION ANN on ANN.ANNOTATED_ID = DV.OBJECT_ID
      where DV.OBJECT_ID = OBJID
      order by ANN.CREATE_DATE desc
    )
    where ROWNUM <= 1) DV
    left join (select
      count(*) CNT, DR."RECORDNUMBER" from FNS_SYS_USER.VWVR1_DEFAULTROSTER DR
    where DR."F_CLASS" not in ('fns-ea-lc-scan', 'fns-ea-lc-scanAndDigitize', 'fns-ea-lc-NOScan', 'fns-ea-lc-store')
    group by DR."RECORDNUMBER") R1 on R1."RECORDNUMBER" = DV.U16D8_RECORDNUMBER
    left join (select
      count(*) CNT, DR."RECORDNUMBER" from FNS_SYS_USER.VWVR1_DEFAULTROSTER DR
    join FNS_SYS_USER.VWVQ1_TASK_TIMECONTROL TC on TC."F_WOBNUM" = DR."F_WOBNUM"
    where DR."F_CLASS" = 'fns-ea-lc-systemTimeController' and TC."TASKNAME" = 'massInputSystemTimeController'
    group by DR."RECORDNUMBER") R3 on R3."RECORDNUMBER" = DV.U16D8_RECORDNUMBER
    where
      ((DV.U05F8_STATUS = 'CREATED' and R1.CNT > 0) or (DV.U05F8_STATUS = 'PROCESSED_UNSUCCESSFULLY') or (DV.U05F8_STATUS = 'PROCESSED' and DV.UAC66_STORAGEPLACETYPE = 2));
    if (STATUS = 'CREATED' and NORSCANNORINPUTNORSTORECNT > 0) then TASKPE := '��������� ���������';
    elsif (STATUS = 'PROCESSED' and STORAGEPT = 2 and CONTROLLAUNCHEDCNT = 0) then TASKPE := '��������� ����������� �� ��������';
    elsif (STATUS = 'PROCESSED' and STORAGEPT = 2 and CONTROLLAUNCHEDCNT > 0) then TASKPE := '��������� ��������';
    elsif (STATUS = 'PROCESSED_UNSUCCESSFULLY') then TASKPE := '������';
    end if;
    return (TASKPE);
  end;
/