create function FNS_UD_EA_COMMON_USER.F$GET_LONG_FILENAME(P_ROWID in ROWID) return VARCHAR2 as
  L_DATA LONG;
  begin
    select FILENAME into L_DATA from FTEWEB.FILE_SPACE_ENTRY where ROWID = P_ROWID;
    return substr(L_DATA, 1, 4000);
  end;
/