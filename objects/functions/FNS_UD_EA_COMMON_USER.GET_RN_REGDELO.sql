create function FNS_UD_EA_COMMON_USER.GET_RN_REGDELO(ID1 in VARCHAR2) return sys_refcursor as
  ID2 VARCHAR2(32);
  ID3 VARCHAR2(32);
  DOCKIND VARCHAR2(10 BYTE);
  OUTPUTDATA sys_refcursor;
  begin
    select
      NVL(DOC.PARENTRECORDNUMBER, DOC.RECORDNUMBER), DOCKIND into ID2, DOCKIND
    from FNS_UD_EA_COMMON_USER.EA_CONVERT_DOC_CSR DOC
    where DOC.RECORDNUMBER = ID1; --and (DOC.CONVERTSTATUS not in (1, 2, 3));
    if DOCKIND <> 'EA_REGDELO' then
      begin
        select NVL(DOC.PARENTRECORDNUMBER, DOC.RECORDNUMBER) into ID3
        from FNS_UD_EA_COMMON_USER.EA_CONVERT_DOC_CSR DOC
        where DOC.RECORDNUMBER = ID2;
      end;
    elsif DOCKIND = 'EA_REGDELO' then ID3 := ID2;
    end if;
    --return ID3;
    open OUTPUTDATA for
    select DOC.* from FNS_UD_EA_COMMON_USER.EA_CONVERT_DOC_CSR DOC
    where DOC.RECORDNUMBER = ID3 or
        DOC.PARENTRECORDNUMBER = ID3
        or
        DOC.PARENTRECORDNUMBER
            in (select DOCCH.RECORDNUMBER from FNS_UD_EA_COMMON_USER.EA_CONVERT_DOC_CSR DOCCH
        where DOCCH.PARENTRECORDNUMBER = ID3);
    return OUTPUTDATA;
    exception
    when no_data_found then
    return null;
  end GET_RN_REGDELO;
/