create function FNS_UD_EA_COMMON_USER.F$DECODE_TU_TYPE(NOTIFICATION_FORM VARCHAR2) return NUMBER as
  RET_VAL NUMBER(1, 0) := 0;
  begin
    case (NOTIFICATION_FORM)
      when '02' then RET_VAL := 1;
    else RET_VAL := 0;
    end case;
    return (RET_VAL);
  end;
/