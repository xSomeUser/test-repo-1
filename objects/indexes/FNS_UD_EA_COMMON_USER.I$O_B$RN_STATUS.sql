create index FNS_UD_EA_COMMON_USER.I$O_B$RN_STATUS on FNS_UD_EA_COMMON_USER.OUTGOING_BUFFER (RECORD_NUMBER, STATUS) parallel 32;
alter index FNS_UD_EA_COMMON_USER.I$O_B$RN_STATUS noparallel;