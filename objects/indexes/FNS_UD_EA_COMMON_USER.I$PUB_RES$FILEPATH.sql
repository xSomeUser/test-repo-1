create index FNS_UD_EA_COMMON_USER.I$PUB_RES$FILEPATH on FNS_UD_EA_COMMON_USER.PUBLISHED_RESOURCE (FILE_PATH) parallel 32;
alter index FNS_UD_EA_COMMON_USER.I$PUB_RES$FILEPATH noparallel;