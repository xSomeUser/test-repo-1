create index AIS3.POOLT_RNUMHASH on FNS_UD_EA_COMMON_USER.EA_CONVERT_DOC_ATTACHE_CSR (DOCRECORDNUMBER, HASH) parallel 32;
alter index AIS3.POOLT_RNUMHASH noparallel;