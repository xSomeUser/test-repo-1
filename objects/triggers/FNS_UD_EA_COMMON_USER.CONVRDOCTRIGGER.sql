create trigger FNS_UD_EA_COMMON_USER.CONVRDOCTRIGGER
before insert on FNS_UD_EA_COMMON_USER.CONV_RDOC
for each row
when (NEW.ID is null)
  begin
    select FNS_UD_EA_COMMON_USER.CONVRDOCSEQ.NEXTVAL
    into :NEW.ID
    from DUAL;
  end;
/