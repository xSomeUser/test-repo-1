create trigger FNS_UD_EA_COMMON_USER.CONVRUNTRIGGER
before insert on FNS_UD_EA_COMMON_USER.CONV_RUN
for each row
  begin
    select FNS_UD_EA_COMMON_USER.CONVRUNSEQ.NEXTVAL
    into :NEW.ID
    from DUAL;
  end;
/