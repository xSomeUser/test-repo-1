create trigger FNS_UD_EA_COMMON_USER.CONVINDFOLDTRIGGER
before insert on FNS_UD_EA_COMMON_USER.CONV_INDEXFOLDERS
for each row
when (NEW.ID is null)
  begin
    select FNS_UD_EA_COMMON_USER.CONVINDFOLDSEQ.NEXTVAL
    into :NEW.ID
    from DUAL;
  end;
/