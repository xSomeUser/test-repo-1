create trigger FNS_UD_EA_COMMON_USER.PROCESS_STATE_TRG
before insert on FNS_UD_EA_COMMON_USER.PROCESS_STATE
for each row
  begin <<COLUMN_SEQUENCES>> begin if inserting and :NEW.ID is null then select PROCESS_STATE_SEQ.NEXTVAL into :NEW.ID from DUAL; end if;
  end COLUMN_SEQUENCES;
  end;
/