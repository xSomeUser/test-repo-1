create trigger FNS_UD_EA_COMMON_USER.CONVMSGTRIGGER
before insert on FNS_UD_EA_COMMON_USER.CONV_MSG
for each row
  begin
    select FNS_UD_EA_COMMON_USER.CONVMSGSEQ.NEXTVAL
    into :NEW.ID
    from DUAL;
  end;
/