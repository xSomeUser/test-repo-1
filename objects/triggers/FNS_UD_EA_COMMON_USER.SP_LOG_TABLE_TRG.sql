create trigger FNS_UD_EA_COMMON_USER.SP_LOG_TABLE_TRG
before insert on FNS_UD_EA_COMMON_USER.SP_LOG_TABLE
for each row
  begin
    <<COLUMN_SEQUENCES>>
    begin
      if inserting and :NEW.ID is null then
        select SP_LOG_TABLE_SEQ.NEXTVAL into :NEW.ID from SYS.DUAL;
      end if;
    end COLUMN_SEQUENCES;
  end;
/