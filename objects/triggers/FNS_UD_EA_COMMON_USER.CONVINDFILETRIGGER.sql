create trigger FNS_UD_EA_COMMON_USER.CONVINDFILETRIGGER
before insert on FNS_UD_EA_COMMON_USER.CONV_INDEXFILES
for each row
when (NEW.ID is null)
  begin
    select FNS_UD_EA_COMMON_USER.CONVINDFILESEQ.NEXTVAL
    into :NEW.ID
    from DUAL;
  end;
/