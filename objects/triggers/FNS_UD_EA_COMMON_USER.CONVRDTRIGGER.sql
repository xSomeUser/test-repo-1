create trigger FNS_UD_EA_COMMON_USER.CONVRDTRIGGER
before insert on FNS_UD_EA_COMMON_USER.CONV_RD
for each row
when (NEW.ID is null)
  begin
    select FNS_UD_EA_COMMON_USER.CONVRDSEQ.NEXTVAL
    into :NEW.ID
    from DUAL;
  end;
/