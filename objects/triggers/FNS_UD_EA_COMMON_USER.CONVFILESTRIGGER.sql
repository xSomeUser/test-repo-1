create trigger FNS_UD_EA_COMMON_USER.CONVFILESTRIGGER
before insert on FNS_UD_EA_COMMON_USER.CONV_FILES
for each row
when (NEW.ID is null)
  begin
    select FNS_UD_EA_COMMON_USER.CONVFILESSEQ.NEXTVAL
    into :NEW.ID
    from DUAL;
  end;
/