create trigger FNS_UD_EA_COMMON_USER.CONVIDTEMPTRIGGER
before
insert on CONV_ID_TEMP
for each row
  begin
    select FNS_UD_EA_COMMON_USER.CONVIDTEMPSEQUENCE.NEXTVAL
    into :NEW.ID
    from DUAL;
  end;
/