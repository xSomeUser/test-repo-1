create trigger FNS_UD_EA_COMMON_USER.CONVIDTEMPSTARTTRIGGER
before update of STARTINDEX on CONV_ID_TEMP
for each row
  begin
    select systimestamp into :NEW.LASTUPDATESTART from DUAL;
  end;
/