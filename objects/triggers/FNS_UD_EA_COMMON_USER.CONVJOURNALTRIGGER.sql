create trigger FNS_UD_EA_COMMON_USER.CONVJOURNALTRIGGER
before insert on FNS_UD_EA_COMMON_USER.CONV_JOURNAL
for each row
  begin
    select FNS_UD_EA_COMMON_USER.CONVJOURNALSEQ.NEXTVAL
    into :NEW.ID
    from DUAL;
  end;
/