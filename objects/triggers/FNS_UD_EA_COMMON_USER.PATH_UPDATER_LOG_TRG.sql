create trigger FNS_UD_EA_COMMON_USER.PATH_UPDATER_LOG_TRG
before insert on FNS_UD_EA_COMMON_USER.PATH_UPDATER_LOG
for each row
  begin
    <<COLUMN_SEQUENCES>>
    begin
      if inserting and :NEW.ID is null then
        select PATH_UPDATER_LOG_SEQ.NEXTVAL into :NEW.ID
        from SYS.DUAL;
      end if;
    end COLUMN_SEQUENCES;
  end;
/