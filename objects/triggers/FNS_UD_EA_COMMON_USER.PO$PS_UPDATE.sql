create trigger FNS_UD_EA_COMMON_USER.PO$PS_UPDATE
before update on FNS_UD_EA_COMMON_USER.PO$PRINT_SCHEDULE
for each row
  begin
    select sysdate into :NEW.MODIFY_DATE from DUAL;
  end;
/