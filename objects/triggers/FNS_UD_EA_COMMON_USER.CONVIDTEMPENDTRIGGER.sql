create trigger FNS_UD_EA_COMMON_USER.CONVIDTEMPENDTRIGGER
before update of ENDINDEX on CONV_ID_TEMP
for each row
  begin
    select systimestamp into :NEW.LASTUPDATEEND from DUAL;
  end;
/