create table FNS_UD_EA_COMMON_USER.CONV_RDOC (
  ID NUMBER(*, 0),
  RKID NUMBER(*, 0) not null,
  IDENTITY VARCHAR2(32 BYTE),
  CREATED VARCHAR2(1 BYTE) default 1 not null,
  LINKED VARCHAR2(1 BYTE) default 0 not null,
  EA_IDENTITY VARCHAR2(100 BYTE),
  ATTACH_ECR NUMBER(6, 0) default 0,
  ATTACH_SOARD NUMBER(6, 0) default 0,
  NOMDOC NVARCHAR2(36),
  constraint CONV_RDOC_PK primary key (ID) enable
);
alter table FNS_UD_EA_COMMON_USER.CONV_RDOC
  add constraint FK_RDOCID foreign key (RKID) references FNS_UD_EA_COMMON_USER.CONV_RK (ID) on delete cascade enable;