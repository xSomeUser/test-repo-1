create table FNS_UD_EA_COMMON_USER.CONV_FAILEDATTACHES (
  ID NUMBER(*, 0) not null,
  DTIME TIMESTAMP(6) default systimestamp,
  INDEXID NUMBER(*, 0),
  INDEXFILEID NUMBER(*, 0),
  RUNID NUMBER(*, 0),
  RDOCID NUMBER(*, 0),
  ERRORID NUMBER(*, 0),
  constraint CONV_FAILEDATTACHES_PK primary key (ID) enable
);