create table FNS_UD_EA_COMMON_USER.CONV_MSG (
  ID NUMBER(*, 0),
  TXT NVARCHAR2(1250),
  ERRORCODE NUMBER default 0,
  RUNID NUMBER default null,
  constraint CONV_MSG_PK primary key (ID) enable
);
alter table FNS_UD_EA_COMMON_USER.CONV_MSG
  add constraint FK_MSGRUNID foreign key (RUNID) references FNS_UD_EA_COMMON_USER.CONV_RUN (ID) on delete cascade enable;