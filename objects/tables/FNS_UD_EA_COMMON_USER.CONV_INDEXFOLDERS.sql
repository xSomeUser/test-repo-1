create table FNS_UD_EA_COMMON_USER.CONV_INDEXFOLDERS (
  ID NUMBER(*, 0) not null,
  INDEXID NUMBER(*, 0) not null,
  FOLDERNAME NVARCHAR2(1024) not null,
  INVALID NUMBER(1, 0) default 0 not null,
  constraint CONV_INDEXFOLDERS_PK primary key (ID) enable
);
alter table FNS_UD_EA_COMMON_USER.CONV_INDEXFOLDERS
  add constraint FK_F_INDEXID foreign key (INDEXID) references FNS_UD_EA_COMMON_USER.CONV_INDEXROOT (ID) on delete cascade enable;