create table FNS_UD_EA_COMMON_USER.CONV_RK (
  ID NUMBER(*, 0),
  RDID NUMBER(*, 0) not null,
  UNID VARCHAR2(50 BYTE) not null,
  STATEREGNUM VARCHAR2(15 BYTE),
  IDENTITY VARCHAR2(32 BYTE),
  CREATED VARCHAR2(1 BYTE) default 1 not null,
  LINKED VARCHAR2(1 BYTE) default 0 not null,
  EA_IDENTITY VARCHAR2(100 BYTE),
  constraint CONV_RK_PK primary key (ID) enable
);
alter table FNS_UD_EA_COMMON_USER.CONV_RK
  add constraint FK_RKID foreign key (RDID) references FNS_UD_EA_COMMON_USER.CONV_RD (ID) on delete cascade enable;