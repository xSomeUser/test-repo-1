create table FNS_UD_EA_COMMON_USER.PO$TASKS (
  ID RAW(16) default SYS_GUID() not null,
  STATUS VARCHAR2(25 BYTE) default 'CREATED' not null,
  CREATE_DATE TIMESTAMP(6) default CURRENT_TIMESTAMP not null,
  MODIFY_DATE TIMESTAMP(6),
  FILE_PATH VARCHAR2(255 BYTE),
  RECEIVER_CODE VARCHAR2(5 BYTE) not null,
  DOC_COUNT NUMBER(10, 0) default 0,
  PRINTING_TYPE NUMBER not null,
  INSTANCE_ID VARCHAR2(25 BYTE),
  LOCK_TIME TIMESTAMP(6) default null,
  RETRY_COUNT NUMBER(*, 0) default 0,
  DEADLINE_DATE DATE,
  TAX_AUTHORITY_CODE VARCHAR2(4 BYTE),
  PO$PRINT_SCHEDULE_ID RAW(16),
  DESCRIPTION VARCHAR2(1024 BYTE),
  EXECUTOR_CODE VARCHAR2(5 BYTE),
  SENT_DATE TIMESTAMP(6)
);
alter table FNS_UD_EA_COMMON_USER.PO$TASKS
  add primary key (ID);
alter table FNS_UD_EA_COMMON_USER.PO$TASKS
  add constraint PO$TASKS_FK1 foreign key (PO$PRINT_SCHEDULE_ID) references FNS_UD_EA_COMMON_USER.PO$PRINT_SCHEDULE (ID) disable;  
comment on table "FNS_UD_EA_COMMON_USER"."PO$TASKS" is 'Таблица формирования заданий на печать';
comment on column "FNS_UD_EA_COMMON_USER"."PO$TASKS"."STATUS" is 'Статус задания на печать';
comment on column "FNS_UD_EA_COMMON_USER"."PO$TASKS"."CREATE_DATE" is 'Дата создания записи';
comment on column "FNS_UD_EA_COMMON_USER"."PO$TASKS"."MODIFY_DATE" is 'Дата изменения записи';
comment on column "FNS_UD_EA_COMMON_USER"."PO$TASKS"."FILE_PATH" is 'Путь до ZIP файла задания';
comment on column "FNS_UD_EA_COMMON_USER"."PO$TASKS"."RECEIVER_CODE" is 'Код ЦПС для отправки задания';
comment on column "FNS_UD_EA_COMMON_USER"."PO$TASKS"."DOC_COUNT" is 'Количество документов в задании';
comment on column "FNS_UD_EA_COMMON_USER"."PO$TASKS"."PRINTING_TYPE" is 'Тип печати для документов в задании';
comment on column "FNS_UD_EA_COMMON_USER"."PO$TASKS"."INSTANCE_ID" is 'Идентификатор инстанса фонового процесса, обрабатывающего данную запись.';
comment on column "FNS_UD_EA_COMMON_USER"."PO$TASKS"."LOCK_TIME" is 'Время блокировки записи.';
comment on column "FNS_UD_EA_COMMON_USER"."PO$TASKS"."RETRY_COUNT" is 'Число попыток обработки таска.';
comment on column "FNS_UD_EA_COMMON_USER"."PO$TASKS"."PO$PRINT_SCHEDULE_ID" is 'ФК на график печати';
comment on column "FNS_UD_EA_COMMON_USER"."PO$TASKS"."DESCRIPTION" is 'Последняя причина ошибки упаковки задания';
comment on column "FNS_UD_EA_COMMON_USER"."PO$TASKS"."EXECUTOR_CODE" is 'Код исполнителя. Из ReestrMailingProtocol';
comment on column "FNS_UD_EA_COMMON_USER"."PO$TASKS"."SENT_DATE" is 'Дата отправки в АИС-ЦОД';