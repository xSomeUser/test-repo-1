create table FNS_UD_EA_COMMON_USER.CONV_INDEXROOT (
  ID NUMBER(*, 0) not null,
  PATHTOROOT NVARCHAR2(1024) not null,
  NAME NVARCHAR2(130),
  DTIME TIMESTAMP(6) default systimestamp,
  INVALID NUMBER(1, 0) default 0 not null,
  TOTALWORKHOURS NUMBER default 0,
  constraint CONV_INDEXROOT_PK primary key (ID) enable
);