create table FNS_UD_EA_COMMON_USER.AU_NOTIFICATION (
  ID NUMBER(38, 0) not null,
  CREATE_DATE TIMESTAMP(6) default CURRENT_TIMESTAMP,
  ERROR_TEXT CLOB,
  STATE NUMBER(10, 0) not null,
  UPDATE_DATE TIMESTAMP(6),
  AU_EVENT_ID NUMBER(38, 0) not null
)
lob (ERROR_TEXT) store as basicfile;
alter table FNS_UD_EA_COMMON_USER.AU_NOTIFICATION
  add primary key (ID) enable;
alter table FNS_UD_EA_COMMON_USER.AU_NOTIFICATION
  add unique (AU_EVENT_ID) enable;