create table FNS_UD_EA_COMMON_USER.CONV_RD (
  ID NUMBER(*, 0),
  FILEID NUMBER(*, 0) not null,
  IDENTITY NVARCHAR2(32),
  TYPE VARCHAR2(2 BYTE) default 'UL' not null,
  CREATED VARCHAR2(1 BYTE) default 1 not null,
  LINKED VARCHAR2(1 BYTE) default 0 not null,
  EA_IDENTITY VARCHAR2(100 BYTE),
  LOADDATE TIMESTAMP(6) default systimestamp,
  NOCODE VARCHAR2(5 BYTE),
  OGRN VARCHAR2(15 BYTE),
  FULLNAME VARCHAR2(1000 BYTE),
  constraint CONV_RD_PK primary key (ID) enable
);
alter table FNS_UD_EA_COMMON_USER.CONV_RD
  add constraint FK_FILEID foreign key (FILEID) references FNS_UD_EA_COMMON_USER.CONV_FILES (ID) on delete cascade enable;