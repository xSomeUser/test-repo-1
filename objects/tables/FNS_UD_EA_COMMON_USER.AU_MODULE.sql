create table FNS_UD_EA_COMMON_USER.AU_MODULE (
  ID NUMBER(38, 0) not null,
  INSTANCE VARCHAR2(758 CHAR) not null,
  NAME VARCHAR2(758 CHAR) not null
);
alter table FNS_UD_EA_COMMON_USER.AU_MODULE
  add primary key (ID) enable;
alter table FNS_UD_EA_COMMON_USER.AU_MODULE
  add unique (NAME, INSTANCE) enable;