create table FNS_UD_EA_COMMON_USER.CONV_RUN (
  ID NUMBER(*, 0) not null,
  DTIME TIMESTAMP(6) default systimestamp,
  INDEXID NUMBER(*, 0),
  PATHTOROOT NVARCHAR2(1024) default '' not null,
  OPNAME NVARCHAR2(1024) default '����� �����' not null,
  ENDDATE TIMESTAMP(6),
  XMLCOUNT NUMBER default 0,
  VAL_GOODXMLCOUNT NUMBER default 0,
  VAL_BADXMLCOUNT NUMBER default 0,
  LR_GOODXMLCOUNT NUMBER default 0,
  LR_BADXMLCOUNT NUMBER default 0,
  AT_GOODXMLCOUNT NUMBER default 0,
  STARTDATE TIMESTAMP(6),
  THREADCOUNT NUMBER default 1,
  TOTALWORKHOURS NUMBER default 0,
  VALWORKHOURS NUMBER default 0,
  constraint CONV_RUN_PK primary key (ID) enable
);