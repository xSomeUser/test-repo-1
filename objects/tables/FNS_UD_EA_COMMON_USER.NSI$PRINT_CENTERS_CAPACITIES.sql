create table FNS_UD_EA_COMMON_USER.NSI$PRINT_CENTERS_CAPACITIES (
  ID RAW(16) default SYS_GUID() not null,
  RECEIVER_CODE VARCHAR2(5 BYTE) not null,
  PRINTING_TYPE NUMBER(3, 0) not null,
  FIO VARCHAR2(128 BYTE),
  CAPACITY NUMBER default 0 not null,
  CREATE_DATE DATE default sysdate not null,
  IS_ANNULLED NUMBER(1, 0) default 0 not null,
  constraint NSI$PRINT_CENTERS_CAPACITI_PK primary key (ID) enable
);
alter table FNS_UD_EA_COMMON_USER.NSI$PRINT_CENTERS_CAPACITIES
  add constraint NSI$PR_CEN_CAP_CHK_ANNULLED check (IS_ANNULLED in (0, 1)) enable;
alter table FNS_UD_EA_COMMON_USER.NSI$PRINT_CENTERS_CAPACITIES
  add constraint NSI$PR_CEN_CAP_CHK_CAPACITY check (CAPACITY >= 0) enable;
comment on table FNS_UD_EA_COMMON_USER.NSI$PRINT_CENTERS_CAPACITIES is 'Справочник мощностей центров печати';
comment on column FNS_UD_EA_COMMON_USER.NSI$PRINT_CENTERS_CAPACITIES.ID is 'Идентификатор записи';
comment on column FNS_UD_EA_COMMON_USER.NSI$PRINT_CENTERS_CAPACITIES.RECEIVER_CODE is 'Код центра печати';
comment on column FNS_UD_EA_COMMON_USER.NSI$PRINT_CENTERS_CAPACITIES.PRINTING_TYPE is 'Тип печати';
comment on column FNS_UD_EA_COMMON_USER.NSI$PRINT_CENTERS_CAPACITIES.FIO is 'ФИО технолога';
comment on column FNS_UD_EA_COMMON_USER.NSI$PRINT_CENTERS_CAPACITIES.CAPACITY is 'Мощность';
comment on column FNS_UD_EA_COMMON_USER.NSI$PRINT_CENTERS_CAPACITIES.CREATE_DATE is 'Дата внесения записи';
comment on column FNS_UD_EA_COMMON_USER.NSI$PRINT_CENTERS_CAPACITIES.IS_ANNULLED is 'Признак аннулирования';
