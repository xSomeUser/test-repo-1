create table FNS_UD_EA_COMMON_USER.AU_EVENT (
  ID NUMBER(38, 0) not null,
  CREATED TIMESTAMP(6) default CURRENT_TIMESTAMP,
  DOC_MODULE VARCHAR2(32 CHAR),
  ERROR_MESSAGE CLOB,
  IP VARCHAR2(15 CHAR),
  MESSAGE CLOB not null,
  METHOD_NAME VARCHAR2(1024 CHAR),
  PROCESS_UUID VARCHAR2(36 CHAR) not null,
  STATUS NUMBER(10, 0) not null,
  TYPE NUMBER(10, 0) not null,
  MODULE_ID NUMBER(38, 0)
)
lob (ERROR_MESSAGE) store as basicfile
lob (MESSAGE) store as basicfile;
alter table FNS_UD_EA_COMMON_USER.AU_EVENT
  add primary key (ID) enable;
alter table FNS_UD_EA_COMMON_USER.AU_EVENT
  add constraint FK96AEEEAFDBAF91F4 foreign key (MODULE_ID) references FNS_UD_EA_COMMON_USER.AU_MODULE (ID) enable;  