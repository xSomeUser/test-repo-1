create table FNS_UD_EA_COMMON_USER.REF$VALUES (
  ID RAW(16) default SYS_GUID() not null,
  REF_CODE VARCHAR2(50 BYTE) not null,
  CODE VARCHAR2(20 BYTE) not null,
  DESCRIPTION VARCHAR2(1024 BYTE) not null,
  constraint REF$VALUES_PK primary key (ID) enable
);
comment on table "FNS_UD_EA_COMMON_USER"."REF$VALUES" is 'Таблица для хранения небольших справочников ЕКП';
comment on column "FNS_UD_EA_COMMON_USER"."REF$VALUES"."ID" is 'Ид. записи';
comment on column "FNS_UD_EA_COMMON_USER"."REF$VALUES"."REF_CODE" is 'Код справочника';
comment on column "FNS_UD_EA_COMMON_USER"."REF$VALUES"."CODE" is 'Код элемента справочника';
comment on column "FNS_UD_EA_COMMON_USER"."REF$VALUES"."DESCRIPTION" is 'Текстовое представление элемента справочника';