create table FNS_UD_EA_COMMON_USER.EA_CONVERT_DOC_CSR (
  RECORDNUMBER VARCHAR2(32 BYTE) not null,
  RECORDDATE DATE not null,
  STORAGEPERIOD NUMBER(3, 0),
  STATUS VARCHAR2(32 BYTE),
  DOCTITLE VARCHAR2(1024 BYTE),
  MILESTONE VARCHAR2(32 BYTE),
  CONVERSIONSIGN NUMBER,
  MAINDOCSIGN NUMBER(1, 0),
  COVERINGLETTERID VARCHAR2(32 BYTE),
  STORAGEPLACETYPE NUMBER(1, 0),
  STORAGEPLACECODE VARCHAR2(5 BYTE),
  POSTDATE DATE,
  DOCPAGECOUNT NUMBER(15, 0),
  ATTACHPAGECOUNT NUMBER(3, 0),
  ATTACHCOUNT NUMBER(3, 0),
  COPIESCOUNT NUMBER(3, 0),
  APPLICANTTYPE VARCHAR2(2 BYTE),
  DOCCODEFROMDIRECTORY VARCHAR2(20 BYTE),
  DOCNUMBER VARCHAR2(25 BYTE),
  SIGNDATE DATE,
  REPRESENTATIONTYPECODE VARCHAR2(2 BYTE),
  RECEIVEDATE TIMESTAMP(6),
  TAXAUTHORITYCODE VARCHAR2(20 BYTE),
  TAXAUTHORITYNAME VARCHAR2(180 BYTE),
  DESCRIPTIONTASK VARCHAR2(20 BYTE),
  INVENTORYNUMBER VARCHAR2(32 BYTE),
  FILENUMBER VARCHAR2(50 BYTE),
  SVPDTYPE VARCHAR2(20 BYTE),
  DOCRECEIPTTYPE NUMBER(1, 0),
  DOCADDPERSONFIO VARCHAR2(1024 BYTE),
  ISSUEDATE DATE,
  ISSUEREASON VARCHAR2(1000 BYTE),
  DOCISSUEPERSONFIO VARCHAR2(1024 BYTE),
  STORAGETYPE VARCHAR2(2 BYTE),
  PAPERSTORAGESTATUS VARCHAR2(2 BYTE),
  ORGNAME VARCHAR2(260 BYTE),
  REGISTRATIONNUMBER VARCHAR2(100 BYTE),
  REGISTRATIONDATE DATE,
  STATEREGNUMBER VARCHAR2(15 BYTE),
  KINDREGISTRATION VARCHAR2(6 BYTE),
  DOCCOUNT NUMBER,
  UNIQUERECORDID VARCHAR2(50 BYTE),
  UNIQUERECORDDATE TIMESTAMP(6),
  OGRN VARCHAR2(13 BYTE),
  OGRNIP VARCHAR2(15 BYTE),
  FULLNAME VARCHAR2(1000 BYTE),
  INN VARCHAR2(12 BYTE),
  KPP VARCHAR2(9 BYTE),
  LASTNAME VARCHAR2(60 BYTE),
  FIRSTNAME VARCHAR2(60 BYTE),
  MIDDLENAME VARCHAR2(60 BYTE),
  PARENTRECORDNUMBER VARCHAR2(32 BYTE),
  ATTACHMENTS VARCHAR2(4000 BYTE),
  CONVERTSTATUS NUMBER default 0 not null,
  DOCKIND VARCHAR2(20 BYTE)
);