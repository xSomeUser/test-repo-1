create table FNS_UD_EA_COMMON_USER.PR_NAME_TRANSLATIONS (
  ORIGINAL VARCHAR2(255 CHAR) not null,
  TRANSLATED VARCHAR2(255 CHAR) not null
);
alter table FNS_UD_EA_COMMON_USER.PR_NAME_TRANSLATIONS
  add primary key (ORIGINAL);