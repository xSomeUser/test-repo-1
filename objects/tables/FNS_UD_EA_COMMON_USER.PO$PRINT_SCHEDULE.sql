create table FNS_UD_EA_COMMON_USER.PO$PRINT_SCHEDULE (
  ID RAW(16) default SYS_GUID() not null,
  NSI$PCC_ID RAW(16) not null,
  START_DATE DATE not null,
  END_DATE DATE not null,
  FIO VARCHAR2(128 BYTE),
  COUNT_SNU NUMBER(*, 0) default 0,
  COUNT_TU NUMBER(*, 0) default 0,
  STATUS VARCHAR2(128 BYTE),
  DATE_STATUS DATE,
  CREATE_DATE DATE default sysdate not null,
  MODIFY_DATE DATE default sysdate not null,
  LOCKED NUMBER(1, 0) default 0 not null,
  COUNT_SNU_TU NUMBER default 0,
  constraint PO$PRINT_SCHEDULE_PK primary key (ID) enable
);
alter table FNS_UD_EA_COMMON_USER.PO$PRINT_SCHEDULE
  add constraint PO$PRINT_SCHEDULE_FK1 foreign key (NSI$PCC_ID) references FNS_UD_EA_COMMON_USER.NSI$PRINT_CENTERS_CAPACITIES (ID) enable;
comment on table "FNS_UD_EA_COMMON_USER"."PO$PRINT_SCHEDULE" is 'Таблица графиков печати';
comment on column "FNS_UD_EA_COMMON_USER"."PO$PRINT_SCHEDULE"."ID" is 'Идентификатор записи';
comment on column "FNS_UD_EA_COMMON_USER"."PO$PRINT_SCHEDULE"."NSI$PCC_ID" is 'Идентификатор записи в таблице NSI$PRINT_CENTERS_CAPACITIES';
comment on column "FNS_UD_EA_COMMON_USER"."PO$PRINT_SCHEDULE"."START_DATE" is 'Дата начала печати';
comment on column "FNS_UD_EA_COMMON_USER"."PO$PRINT_SCHEDULE"."END_DATE" is 'Дата окончания печати';
comment on column "FNS_UD_EA_COMMON_USER"."PO$PRINT_SCHEDULE"."FIO" is 'ФИО технолога';
comment on column "FNS_UD_EA_COMMON_USER"."PO$PRINT_SCHEDULE"."COUNT_SNU" is 'Количество СНУ';
comment on column "FNS_UD_EA_COMMON_USER"."PO$PRINT_SCHEDULE"."COUNT_TU" is 'Количество ТУ';
comment on column "FNS_UD_EA_COMMON_USER"."PO$PRINT_SCHEDULE"."STATUS" is 'Статус';
comment on column "FNS_UD_EA_COMMON_USER"."PO$PRINT_SCHEDULE"."DATE_STATUS" is 'Дата перехода в статус';
comment on column "FNS_UD_EA_COMMON_USER"."PO$PRINT_SCHEDULE"."CREATE_DATE" is 'Дата создания графика (по умолчанию текущая дата)';
comment on column "FNS_UD_EA_COMMON_USER"."PO$PRINT_SCHEDULE"."MODIFY_DATE" is 'Дата изменения графика (по умолчанию проставляется дата изменения)';
comment on column "FNS_UD_EA_COMMON_USER"."PO$PRINT_SCHEDULE"."COUNT_SNU_TU" is 'Кол-во объединенных ПО';