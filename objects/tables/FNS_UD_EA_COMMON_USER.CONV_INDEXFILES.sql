create table FNS_UD_EA_COMMON_USER.CONV_INDEXFILES (
  ID NUMBER(*, 0) not null,
  FOLDERID NUMBER(*, 0) not null,
  FILENAME NVARCHAR2(512) not null,
  FILEEXT NVARCHAR2(96),
  ATTACHED NUMBER(1, 0) default 0 not null,
  INVALID NUMBER(1, 0) default 0 not null,
  DOCID VARCHAR2(32 CHAR),
  constraint CONV_INDEXFILES_PK primary key (ID) enable
);
alter table FNS_UD_EA_COMMON_USER.CONV_INDEXFILES
  add constraint FK_FOLDERID foreign key (FOLDERID) references FNS_UD_EA_COMMON_USER.CONV_INDEXFOLDERS (ID) on delete cascade enable;