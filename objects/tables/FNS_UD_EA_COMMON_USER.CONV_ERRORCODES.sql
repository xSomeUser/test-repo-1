create table FNS_UD_EA_COMMON_USER.CONV_ERRORCODES (
  ID NUMBER(*, 0) not null,
  TEXT NVARCHAR2(1024) not null,
  RUNID NUMBER not null,
  constraint CONV_ERRORCODES_PK primary key (ID) enable
);