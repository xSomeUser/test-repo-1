create table FNS_UD_EA_COMMON_USER.AU_JOURNAL (
  ID NUMBER(38, 0) not null,
  NAME VARCHAR2(255 CHAR) not null,
  REQUEST_CLASS_NAME VARCHAR2(1024 CHAR) not null,
  REQUEST_JSON CLOB not null
)
lob (REQUEST_JSON) store as basicfile;
alter table FNS_UD_EA_COMMON_USER.AU_JOURNAL
  add primary key (ID) enable;
alter table FNS_UD_EA_COMMON_USER.AU_JOURNAL
  add unique (NAME) enable;