create table FNS_UD_EA_COMMON_USER.PO$IR_RESEND_TEMP (
  ID RAW(16) not null,
  STATUS NUMBER(1, 0) default 0 not null,
  constraint PO$IR_RESEND_TEMP_PK primary key (ID) enable
);
comment on column "FNS_UD_EA_COMMON_USER"."PO$IR_RESEND_TEMP"."ID" is 'Ид. ИР';
comment on column "FNS_UD_EA_COMMON_USER"."PO$IR_RESEND_TEMP"."STATUS" is 'Этап обработки';
