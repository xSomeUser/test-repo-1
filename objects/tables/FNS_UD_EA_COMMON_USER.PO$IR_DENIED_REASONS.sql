create table FNS_UD_EA_COMMON_USER.PO$IR_DENIED_REASONS (
  ID RAW(16) default SYS_GUID() not null,
  RECORD_NUMBER VARCHAR2(32 BYTE) not null,
  REASON_CODE NUMBER(3, 0) not null,
  REASON_DESCRIPTION VARCHAR2(255 BYTE) not null,
  CREATE_DATE DATE default SYSDATE not null,
  constraint PO$IR_DENIED_REASONS_PK primary key (ID) enable
);
comment on table "FNS_UD_EA_COMMON_USER"."PO$IR_DENIED_REASONS" is 'Отклоненные документы для ИР ПО';
comment on column "FNS_UD_EA_COMMON_USER"."PO$IR_DENIED_REASONS"."ID" is 'Ид. записи';
comment on column "FNS_UD_EA_COMMON_USER"."PO$IR_DENIED_REASONS"."RECORD_NUMBER" is 'Учетный номер документа';
comment on column "FNS_UD_EA_COMMON_USER"."PO$IR_DENIED_REASONS"."REASON_CODE" is 'Код причины отклонения';
comment on column "FNS_UD_EA_COMMON_USER"."PO$IR_DENIED_REASONS"."REASON_DESCRIPTION" is 'Описание причины отклонения';
comment on column "FNS_UD_EA_COMMON_USER"."PO$IR_DENIED_REASONS"."CREATE_DATE" is 'Дата занесения';