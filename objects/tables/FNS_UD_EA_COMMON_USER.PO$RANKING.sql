create table FNS_UD_EA_COMMON_USER.PO$RANKING (
  ID RAW(16) not null,
  A1 FLOAT(126),
  A2 FLOAT(126),
  A3 FLOAT(126),
  A4 FLOAT(126),
  A5 FLOAT(126),
  A5_K NUMBER(*, 0),
  IS_DIRTY NUMBER(*, 0) default 0 not null,
  MODIFY_DATE DATE default SYSDATE not null,
  constraint PO$RANKING_PK primary key (ID) enable,
  constraint PO$RANKING_CHK1 check (IS_DIRTY = 0 or IS_DIRTY = 1) enable
);
comment on table "FNS_UD_EA_COMMON_USER"."PO$RANKING" is 'Коэффициенты итогового приоритета';
comment on column "FNS_UD_EA_COMMON_USER"."PO$RANKING"."ID" is 'Идентификатор(Уч. номер документа)';
comment on column "FNS_UD_EA_COMMON_USER"."PO$RANKING"."A1" is 'Приоритет по ф-ции А1.1';
comment on column "FNS_UD_EA_COMMON_USER"."PO$RANKING"."A2" is 'Приоритет по ф-ции А1.2';
comment on column "FNS_UD_EA_COMMON_USER"."PO$RANKING"."A3" is 'Приоритет по ф-ции А1.3';
comment on column "FNS_UD_EA_COMMON_USER"."PO$RANKING"."A4" is 'Приоритет по ф-ции А1.4';
comment on column "FNS_UD_EA_COMMON_USER"."PO$RANKING"."A5" is 'Приоритет по ф-ции А2';
comment on column "FNS_UD_EA_COMMON_USER"."PO$RANKING"."A5_K" is 'Коэфф. для расчета A2';
comment on column "FNS_UD_EA_COMMON_USER"."PO$RANKING"."IS_DIRTY" is 'Признак пересчета';
comment on column "FNS_UD_EA_COMMON_USER"."PO$RANKING"."MODIFY_DATE" is 'Дата модификации';