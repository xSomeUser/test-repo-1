create table FNS_UD_EA_COMMON_USER.SP_LOG_TABLE (
  ID NUMBER not null,
  DATATIME TIMESTAMP(6),
  MESSAGE VARCHAR2(255 BYTE),
  constraint SP_LOG_TABLE_PK primary key (ID) enable
);