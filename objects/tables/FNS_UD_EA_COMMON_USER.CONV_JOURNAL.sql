create table FNS_UD_EA_COMMON_USER.CONV_JOURNAL (
  ID NUMBER(*, 0) not null,
  RUNID NUMBER(*, 0) not null,
  OBJTYPE NUMBER(1, 0) not null,
  OBJID NUMBER(*, 0) not null,
  ACTION NUMBER(2, 0) not null,
  constraint CONV_JOURNAL_PK primary key (ID) enable
);