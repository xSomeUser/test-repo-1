create table FNS_UD_EA_COMMON_USER.TR$TECH_INBOX_ENTITY (
  ID RAW(16) default SYS_GUID() not null,
  CREATE_DATE TIMESTAMP(6),
  MODIFY_DATE TIMESTAMP(6),
  SYSTEM_NAME VARCHAR2(16 BYTE),
  INSTANCE_ID VARCHAR2(32 BYTE),
  SENDER_NAME VARCHAR2(512 BYTE),
  FILE_NAME VARCHAR2(512 BYTE),
  URL_PATH VARCHAR2(512 BYTE),
  DOCUMENT_TYPE VARCHAR2(64 BYTE),
  STATUS VARCHAR2(32 BYTE),
  ERROR_CODE NUMBER(3, 0),
  MESSAGE VARCHAR2(1024 BYTE),
  LOCK_TIME TIMESTAMP(6),
  RETRY_COUNT NUMBER(3, 0),
  IS_RETRYING NUMBER(1, 0),
  SEND_DATE TIMESTAMP(6),
  constraint TR$TECH_INBOX_ENTITY_PK primary key (ID) enable
);