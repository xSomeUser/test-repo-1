create table FNS_UD_EA_COMMON_USER.CB$COMMAND_QUEUE (
  ID NUMBER(38, 0) not null,
  COMMAND_TYPE VARCHAR2(255 CHAR) not null,
  CREATE_DATE TIMESTAMP(6) default CURRENT_TIMESTAMP not null,
  DELAYED_TIME TIMESTAMP(6),
  ERROR_CODE NUMBER(10, 0) not null,
  RUN_COUNT NUMBER(10, 0) not null,
  SERIALIZED_DATA BLOB,
  STATUS VARCHAR2(255 CHAR) not null
)
lob (SERIALIZED_DATA) store as basicfile;
alter table FNS_UD_EA_COMMON_USER.CB$COMMAND_QUEUE
  add primary key (ID);
comment on table "FNS_UD_EA_COMMON_USER"."CB$COMMAND_QUEUE" is 'Будущая разработка для реализации шины событий  и их обработки';