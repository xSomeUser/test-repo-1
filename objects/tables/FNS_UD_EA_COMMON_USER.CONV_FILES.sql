create table FNS_UD_EA_COMMON_USER.CONV_FILES (
  ID NUMBER(*, 0) not null,
  RUNID NUMBER(*, 0) not null,
  PROCESSED NUMBER(1, 0) default 0 not null,
  FILEPATH NVARCHAR2(1024) not null,
  ERRORID NUMBER(*, 0),
  PROCESSING NUMBER(1, 0) default 0 not null,
  constraint CONV_FILES_PK primary key (ID) enable
);