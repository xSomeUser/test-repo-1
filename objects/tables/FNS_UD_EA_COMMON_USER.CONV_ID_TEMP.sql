create table FNS_UD_EA_COMMON_USER.CONV_ID_TEMP (
  ID NUMBER not null,
  TABLENAME VARCHAR2(256 CHAR) not null,
  STARTINDEX NUMBER,
  ENDINDEX NUMBER,
  LASTUPDATESTART TIMESTAMP(6) default systimestamp not null,
  LASTUPDATEEND TIMESTAMP(6) default systimestamp not null
);