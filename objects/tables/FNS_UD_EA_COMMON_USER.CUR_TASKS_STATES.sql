create table FNS_UD_EA_COMMON_USER.CUR_TASKS_STATES (
  ID NUMBER(10, 0) not null,
  TRANSLATED VARCHAR2(255 CHAR) not null
);
alter table FNS_UD_EA_COMMON_USER.CUR_TASKS_STATES
  add primary key (ID);