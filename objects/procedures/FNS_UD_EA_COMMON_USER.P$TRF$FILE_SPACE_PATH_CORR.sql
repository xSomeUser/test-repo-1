create procedure FNS_UD_EA_COMMON_USER.P$TRF$FILE_SPACE_PATH_CORR as
  SEARCH_TEMLATE VARCHAR2(1000) := '//dpc.tax.nalog.ru/root/GRs_UDEA/gr216';
  REPLACE_TEMLATE VARCHAR2(1000) := '//dpc.tax.nalog.ru/root/GRs_UDEA/gr216';
  MAX_ROWS_PROCESSED NUMBER(20, 0) := 100;
  begin
    update FTEWEB.FILE_SPACE_ENTRY
    set FILENAME = REPLACE(FNS_UD_EA_COMMON_USER.F$GET_LONG_FILENAME(ROWID), SEARCH_TEMLATE, REPLACE_TEMLATE)
    where ID in (select ID from (select ID from FTEWEB.FILE_SPACE_ENTRY
    where FNS_UD_EA_COMMON_USER.F$GET_LONG_FILENAME(ROWID) like SEARCH_TEMLATE || '%')
    where ROWNUM <= MAX_ROWS_PROCESSED);
  end;
/