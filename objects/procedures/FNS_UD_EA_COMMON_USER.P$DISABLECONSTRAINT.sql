create procedure FNS_UD_EA_COMMON_USER.P$DISABLECONSTRAINT(T_NAME VARCHAR2) as
  begin
    SYS.DBMS_OUTPUT.PUT_LINE('Disable constraint!!!');
    --���� �� ������ ������
    for TABLECUR in (select
      OWNER, TABLE_NAME from ALL_TABLES
    where (upper(TABLE_NAME) like upper(T_NAME))
        and OWNER = 'FNS_UD_EA_COMMON_USER') loop
      --����� �������� ������ (��� ������� � ���������� ������� � ������)
      --execute immediate 'select count(1) from '||tableCur.owner||'.'||tableCur.TABLE_NAME into cnt;
      --SYS.DBMS_OUTPUT.PUT_LINE('table_name = '||tableCur.TABLE_NAME||', cnt = '||cnt);
      SYS.DBMS_OUTPUT.PUT_LINE('Execute table "' || TABLECUR.TABLE_NAME || '"');
      --���� �� ����������� � ������ �������
      for CUR in (select
        FK.OWNER, FK.CONSTRAINT_NAME, FK.TABLE_NAME
      from ALL_CONSTRAINTS FK, ALL_CONSTRAINTS PK
      where FK.CONSTRAINT_TYPE = 'R' and
          PK.OWNER = TABLECUR.OWNER and
          FK.R_OWNER = PK.OWNER and
          FK.R_CONSTRAINT_NAME = PK.CONSTRAINT_NAME and
          PK.TABLE_NAME = TABLECUR.TABLE_NAME) loop
        --����� �������� ������ (��� ����������)
        --SYS.DBMS_OUTPUT.PUT_LINE('constraint_name = '||cur.constraint_name);
        SYS.DBMS_OUTPUT.PUT_LINE('  Disable constraint "' || CUR.CONSTRAINT_NAME || '"');
        --���������� ���������� � ������� �������
        execute immediate 'ALTER TABLE "' || CUR.OWNER || '"."' || CUR.TABLE_NAME || '" MODIFY CONSTRAINT "' || CUR.CONSTRAINT_NAME || '" DISABLE';
      end loop;
      SYS.DBMS_OUTPUT.PUT_LINE('  Disable triggers "' || TABLECUR.TABLE_NAME || '"');
      execute immediate 'alter table "' || TABLECUR.OWNER || '"."' || TABLECUR.TABLE_NAME || '" disable all triggers';
    end loop;
  end P$DISABLECONSTRAINT;
/