create procedure FNS_UD_EA_COMMON_USER.P$PO_IR_RESEND as
  L_CNT_IRS NUMBER;
  L_DOC_CNT NUMBER;
  L_TASK_ID RAW(16);
  L_COMPONENTNAME VARCHAR2(50) := 'ru.fns.common.schedule.IRPO$RESEND';
  L_CURRENT_DATE DATE := SYSDATE;
  L_GUID RAW(16) := SYS_GUID;
  L_STARTDT TIMESTAMP := CURRENT_TIMESTAMP;
  L_BEGINDATE TIMESTAMP := TO_TIMESTAMP('01.01.1970', 'dd.MM.YYYY HH24:mi');
  L_TOTAL_CNT NUMBER := 0;
  L_ERR VARCHAR2(1024);
  begin
    /*������������ ������ ������������*/
    insert into FNS_UD_EA_COMMON_USER.PROCESS_STATUS
    (ID, COMPONENT_NAME, DESCRIPTION, COMPONENT_TYPE, ERROR, STEP, TIME_STAMP, THREAD_ID, ELEMENTS, DURATION)
    values (FNS_UD_EA_COMMON_USER.HIBERNATE_SEQUENCE.NEXTVAL, L_COMPONENTNAME, 'Process started', 0, 0, 'start', CURRENT_TIMESTAMP,
            L_GUID, 0, L_BEGINDATE + (CURRENT_TIMESTAMP - L_STARTDT));
    commit;
    delete from FNS_UD_EA_COMMON_USER.PO$IR_RESEND_TEMP;
    /* ��������� �� ��������� ������� ID� ������ ���� */
    insert into FNS_UD_EA_COMMON_USER.PO$IR_RESEND_TEMP
    (ID, STATUS)
      select
        IR.ID, 0 from FNS_UD_EA_COMMON_USER.PO$INFORMATION_RESOURCE IR
      join FNS_UD_EA_COMMON_USER.PO$IR_DENIED_REASONS DR on (IR.RECORD_NUMBER = DR.RECORD_NUMBER)
      where IR.STATUS in ('SENT_TO_PRINT', 'NOT_ACCEPTED')
          and IR.PO$TASK_ID is not null
          and IR.PO$PRINT_SCHEDULE_ID is not null
          and DR.REASON_CODE = 150;
    DBMS_OUTPUT.put_line('�������� ' || sql%rowcount || ' ���� ��� ������������');
    /* �������� ��� ERROR ��� � retry_count = 5 */
    update FNS_UD_EA_COMMON_USER.PO$INFORMATION_RESOURCE
    set STATUS = 'ERROR'
    where ID in (select ID from FNS_UD_EA_COMMON_USER.PO$IR_RESEND_TEMP) and
        RETRY_COUNT = 5;
    if sql%rowcount > 0 then
      delete from FNS_UD_EA_COMMON_USER.PO$IR_RESEND_TEMP
      where ID in (
        select T.ID from FNS_UD_EA_COMMON_USER.PO$IR_RESEND_TEMP T
        join FNS_UD_EA_COMMON_USER.PO$INFORMATION_RESOURCE IR on (T.ID = IR.ID)
        where IR.STATUS = 'ERROR'
      );
    end if;
    for IRS in ( select
      IR.PO$PRINT_SCHEDULE_ID, IR.RECEIVER_CODE, IR.TAX_AUTHORITY_CODE, IR.PRINTING_TYPE, Trunc(PS.END_DATE) DD, count(*) CNT
    from FNS_UD_EA_COMMON_USER.PO$INFORMATION_RESOURCE IR
    join FNS_UD_EA_COMMON_USER.PO$IR_RESEND_TEMP on (PO$IR_RESEND_TEMP.ID = IR.ID)
    join FNS_UD_EA_COMMON_USER.PO$PRINT_SCHEDULE PS on (PS.ID = IR.PO$PRINT_SCHEDULE_ID)
    group by IR.PO$PRINT_SCHEDULE_ID, IR.RECEIVER_CODE, IR.TAX_AUTHORITY_CODE, IR.PRINTING_TYPE, Trunc(PS.END_DATE))
    loop
      DBMS_OUTPUT.put_line('������������  RECEIVER_CODE=' || IRS.RECEIVER_CODE || ' TAX_AUTHORITY_CODE=' || IRS.TAX_AUTHORITY_CODE || ' PO$PRINT_SCHEDULE_ID=' || IRS.PO$PRINT_SCHEDULE_ID || ' DEADLINE_DATE=' || IRS.DD);
      L_CNT_IRS := IRS.CNT;
      while L_CNT_IRS > 0
      loop
        if L_CNT_IRS > 10000 then
          L_DOC_CNT := 10000;
        else
          L_DOC_CNT := L_CNT_IRS;
        end if;
        L_TASK_ID := SYS_GUID();
        /* ������� ����� ���� */
        insert into FNS_UD_EA_COMMON_USER.PO$TASKS
        (
          ID,
          STATUS,
          CREATE_DATE,
          MODIFY_DATE,
          FILE_PATH,
          RECEIVER_CODE,
          DOC_COUNT,
          PRINTING_TYPE,
          INSTANCE_ID,
          LOCK_TIME,
          RETRY_COUNT,
          DEADLINE_DATE,
          TAX_AUTHORITY_CODE,
          PO$PRINT_SCHEDULE_ID
        )
        values
          (
            L_TASK_ID,
            'CREATED',
            SYSDATE,
            SYSDATE,
            null, --l_file_path||RAWTOHEX(l_task_id),
            IRS.RECEIVER_CODE,
            L_DOC_CNT,
            IRS.PRINTING_TYPE,
            null, --'inst_X',
            null,
            1,
            IRS.DD,
            IRS.TAX_AUTHORITY_CODE,
            IRS.PO$PRINT_SCHEDULE_ID
          );
        /* �������� ������� ����� */
        update FNS_UD_EA_COMMON_USER.PO$IR_RESEND_TEMP
        set STATUS = 1
        where ID in (select IR.ID from FNS_UD_EA_COMMON_USER.PO$INFORMATION_RESOURCE IR
        join FNS_UD_EA_COMMON_USER.PO$IR_RESEND_TEMP T on (T.ID = IR.ID)
        where
          IRS.RECEIVER_CODE = IR.RECEIVER_CODE
              --AND irs.DD = TRUNC(ir.DEADLINE_DATE)
              and IRS.TAX_AUTHORITY_CODE = IR.TAX_AUTHORITY_CODE
              and IRS.PO$PRINT_SCHEDULE_ID = IR.PO$PRINT_SCHEDULE_ID
              and IRS.PRINTING_TYPE = IR.PRINTING_TYPE
              and ROWNUM <= L_DOC_CNT
        );
        DBMS_OUTPUT.put_line('������ ����� - ' || sql%rowcount);
        /* ��������� ������� ����� � ������ ������ */
        for TSK in ( select
          IR.PO$TASK_ID, Count(*) CNT
        from FNS_UD_EA_COMMON_USER.PO$INFORMATION_RESOURCE IR
        join FNS_UD_EA_COMMON_USER.PO$IR_RESEND_TEMP T on (IR.ID = T.ID)
        where T.STATUS = 1
        group by IR.PO$TASK_ID)
        loop
          update FNS_UD_EA_COMMON_USER.PO$TASKS
          set DOC_COUNT = DOC_COUNT - TSK.CNT
          where ID = TSK.PO$TASK_ID;
        end loop;
        /* ����������� ��� �� ����� ���� */
        update FNS_UD_EA_COMMON_USER.PO$INFORMATION_RESOURCE
        set STATUS = 'IN_PRINT_SCHEDULE',
          PO$TASK_ID = L_TASK_ID,
          RETRY_COUNT = RETRY_COUNT + 1
        where
          ID in (
            select ID from FNS_UD_EA_COMMON_USER.PO$IR_RESEND_TEMP where STATUS = 1
          );
        DBMS_OUTPUT.put_line('����������� ���� - ' || sql%rowcount);
        /* ������ ������ ����� �� ��������� */
        update FNS_UD_EA_COMMON_USER.PO$TASKS
        set STATUS = 'FORMED'
        where ID = L_TASK_ID;
        /* ��������� ������ �� PO$IR_DENIED_REASONS (���������� ��������� ��������� �������� ��������� ��-�� ������ ��)*/
        /*
        delete from FNS_UD_EA_COMMON_USER.PO$IR_DENIED_REASONS
         where RECORD_NUMBER in ( select ir.RECORD_NUMBER from FNS_UD_EA_COMMON_USER.PO$IR_RESEND_TEMP t
                                     join  FNS_UD_EA_COMMON_USER.PO$INFORMATION_RESOURCE ir on (t.Id = ir.Id)
                                       where t.STATUS = 1
                                );
        */
        update FNS_UD_EA_COMMON_USER.PO$IR_DENIED_REASONS
        set REASON_CODE = 666
        where RECORD_NUMBER in (select IR.RECORD_NUMBER from FNS_UD_EA_COMMON_USER.PO$IR_RESEND_TEMP T
        join FNS_UD_EA_COMMON_USER.PO$INFORMATION_RESOURCE IR on (T.ID = IR.ID)
        where T.STATUS = 1
        )
            and REASON_CODE = 150;
        /* ������ ��������� ������� �� ����� */
        delete from FNS_UD_EA_COMMON_USER.PO$IR_RESEND_TEMP
        where STATUS = 1;
        L_CNT_IRS := L_CNT_IRS - L_DOC_CNT;
      end loop;
      L_TOTAL_CNT := L_TOTAL_CNT + IRS.CNT;
    end loop;
    commit;
    DBMS_OUTPUT.put_line('���������� ������������');
    /*������������ ��������� ������������*/
    insert into FNS_UD_EA_COMMON_USER.PROCESS_STATUS
    (ID, COMPONENT_NAME, DESCRIPTION, COMPONENT_TYPE, ERROR, STEP, TIME_STAMP, THREAD_ID, ELEMENTS, DURATION)
    values (FNS_UD_EA_COMMON_USER.HIBERNATE_SEQUENCE.NEXTVAL, L_COMPONENTNAME, 'OK', 0, 0, 'end', CURRENT_TIMESTAMP,
            L_GUID, L_TOTAL_CNT, L_BEGINDATE + (CURRENT_TIMESTAMP - L_STARTDT));
    commit;
    exception
    when others then
    L_ERR := SUBSTR(SQLERRM, 1, 1024);
    rollback;
    /*������������ ������ ������������*/
    insert into FNS_UD_EA_COMMON_USER.PROCESS_STATUS
    (ID, COMPONENT_NAME, DESCRIPTION, COMPONENT_TYPE, ERROR, STEP, TIME_STAMP, THREAD_ID, ELEMENTS, DURATION)
    values (FNS_UD_EA_COMMON_USER.HIBERNATE_SEQUENCE.NEXTVAL, L_COMPONENTNAME, L_ERR, 0, 1, 'end', CURRENT_TIMESTAMP,
            L_GUID, 0, L_BEGINDATE + (CURRENT_TIMESTAMP - L_STARTDT));
    commit;
    raise;
  end;
/