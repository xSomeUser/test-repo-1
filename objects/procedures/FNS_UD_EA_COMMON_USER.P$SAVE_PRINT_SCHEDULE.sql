create procedure FNS_UD_EA_COMMON_USER.P$SAVE_PRINT_SCHEDULE(PS_ID_ in VARCHAR2, IDS_ in VARCHAR2) as
  SQL_ VARCHAR2(32767);
  begin
    SQL_ := 'update FNS_UD_EA_COMMON_USER.PO$INFORMATION_RESOURCE set Status=''IN_SENDING_QUEUE''';
    SQL_ := SQL_ || ' where PARENT_RN in (select RECORD_NUMBER from FNS_UD_EA_COMMON_USER.PO$INFORMATION_RESOURCE where RAWTOHEX(Id) in (' || IDS_ || '))';
    execute immediate SQL_;
    SQL_ := 'update FNS_UD_EA_COMMON_USER.PO$INFORMATION_RESOURCE set PO$PRINT_SCHEDULE_ID=HEXTORAW(''' || PS_ID_ || '''), Status=''IN_SENDING_QUEUE'',MANUAL_EDIT=1';
    SQL_ := SQL_ || ' where RAWTOHEX(Id) in (' || IDS_ || ')';
    execute immediate SQL_;
    SQL_ := 'update FNS_UD_EA_COMMON_USER.PO$INFORMATION_RESOURCE set Status=''CREATED''';
    SQL_ := SQL_ || ' where PARENT_RN in (select RECORD_NUMBER from FNS_UD_EA_COMMON_USER.PO$INFORMATION_RESOURCE where RAWTOHEX(Id) not in (' || IDS_ || ') and PO$PRINT_SCHEDULE_ID = HEXTORAW(''' || PS_ID_ || '''))';
    execute immediate SQL_;
    SQL_ := 'update FNS_UD_EA_COMMON_USER.PO$INFORMATION_RESOURCE set PO$PRINT_SCHEDULE_ID=null, Status=''CREATED'',MANUAL_EDIT=0';
    SQL_ := SQL_ || ' where RAWTOHEX(Id) not in (' || IDS_ || ') and PO$PRINT_SCHEDULE_ID = HEXTORAW(''' || PS_ID_ || ''')';
    execute immediate SQL_;
    update FNS_UD_EA_COMMON_USER.PO$PRINT_SCHEDULE
    set COUNT_SNU = (select count(*) from FNS_UD_EA_COMMON_USER.PO$INFORMATION_RESOURCE where PO$PRINT_SCHEDULE_ID = HEXTORAW(PS_ID_) and NOTIFICATION_FORM = '01'),
      COUNT_TU = (select count(*) from FNS_UD_EA_COMMON_USER.PO$INFORMATION_RESOURCE where PO$PRINT_SCHEDULE_ID = HEXTORAW(PS_ID_) and NOTIFICATION_FORM = '02'),
      COUNT_SNU_TU = (select count(*) from FNS_UD_EA_COMMON_USER.PO$INFORMATION_RESOURCE where PO$PRINT_SCHEDULE_ID = HEXTORAW(PS_ID_) and DELIVERY_TYPE = 1)
    where ID = HEXTORAW(PS_ID_);
  end;
/