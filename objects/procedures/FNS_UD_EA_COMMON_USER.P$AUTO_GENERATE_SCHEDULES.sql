create procedure FNS_UD_EA_COMMON_USER.P$AUTO_GENERATE_SCHEDULES(START_DATE_ DATE, END_DATE_ DATE) as
  FIO_ VARCHAR2(50) := '������������� �������';
  PS_ID_ RAW(16);
  CAPACITY_ NUMBER(11, 0);
  CNT_ NUMBER(11, 0);
  CPS_ VARCHAR(5);
  PRINTING_TYPE_ NUMBER(3, 0);
  begin
    --��� ����� - ��������� ������.
    --start_date_ := TRUNC(SYSDATE,'d') + 7;
    --end_date_ := TRUNC(SYSDATE,'d') + 7 + 6;
    for PCC in ( select * from FNS_UD_EA_COMMON_USER.NSI$PRINT_CENTERS_CAPACITIES
    where IS_ANNULLED = 0) loop
      CPS_ := PCC.RECEIVER_CODE;
      PRINTING_TYPE_ := PCC.PRINTING_TYPE;
      CAPACITY_ := PCC.CAPACITY;
      DBMS_OUTPUT.put_line('��������� �� ��� ' || CPS_ || ' PR_TYPE=' || PRINTING_TYPE_);
      select COUNT(*) into CNT_ from FNS_UD_EA_COMMON_USER.PO$INFORMATION_RESOURCE
      where STATUS = 'CREATED' and
          PARENT_RN is null and
          PRINTING_TYPE = PRINTING_TYPE_ and
          RECEIVER_CODE = CPS_ and
          ROWNUM <= 1;
      continue when CNT_ <> 1;
      DBMS_OUTPUT.put_line('������� ������ ��� ' || CPS_ || ' PR_TYPE=' || PRINTING_TYPE_);
      insert into FNS_UD_EA_COMMON_USER.PO$PRINT_SCHEDULE
      (
        NSI$PCC_ID,
        START_DATE,
        END_DATE,
        FIO,
        COUNT_SNU,
        COUNT_TU,
        STATUS,
        DATE_STATUS,
        CREATE_DATE,
        MODIFY_DATE,
        LOCKED
      )
      values
        (
          PCC.ID,
          START_DATE_,
          END_DATE_,
          FIO_,
          0,
          0,
          'PREPARED',
          SYSDATE,
          SYSDATE,
          SYSDATE,
          0
        ) returning ID into ps_id_;
      update FNS_UD_EA_COMMON_USER.PO$INFORMATION_RESOURCE
      set PO$PRINT_SCHEDULE_ID = PS_ID_,
        STATUS = 'IN_SENDING_QUEUE'
      where ID in (select ID from (select ID from FNS_UD_EA_COMMON_USER.PO$INFORMATION_RESOURCE
      where STATUS = 'CREATED' and
          PRINTING_TYPE = PRINTING_TYPE_ and
          PARENT_RN is null and
          RECEIVER_CODE = CPS_
      order by NVL(ACTUAL_PRIORITY, 0) desc) where ROWNUM <= CAPACITY_);
      CNT_ := sql%rowcount;
      -- ������������ ���� �����������.
      update FNS_UD_EA_COMMON_USER.PO$INFORMATION_RESOURCE
      set
        --PO$PRINT_SCHEDULE_ID = ps_id_,
        STATUS = 'IN_SENDING_QUEUE'
      where STATUS = 'CREATED' and PARENT_RN in (select RECORD_NUMBER from FNS_UD_EA_COMMON_USER.PO$INFORMATION_RESOURCE
      where PO$PRINT_SCHEDULE_ID = PS_ID_ and DELIVERY_TYPE = 1);
      DBMS_OUTPUT.put_line('���-�� �� ' || CNT_);
      update FNS_UD_EA_COMMON_USER.PO$PRINT_SCHEDULE
      set COUNT_SNU = (select count(*) from FNS_UD_EA_COMMON_USER.PO$INFORMATION_RESOURCE where PO$PRINT_SCHEDULE_ID = PS_ID_ and NOTIFICATION_FORM = '01'),
        COUNT_TU = (select count(*) from FNS_UD_EA_COMMON_USER.PO$INFORMATION_RESOURCE where PO$PRINT_SCHEDULE_ID = PS_ID_ and NOTIFICATION_FORM = '02')
      where ID = PS_ID_;
    end loop;
  end;
/