create procedure FNS_UD_EA_COMMON_USER.P$TRF$DOCVERSION_PATH_CORR as
  SEARCH_TEMLATE VARCHAR2(1000) := '\\\\dpc.tax.nalog.ru\\root\\GRs_UDEA\\gr216';
  REPLACE_TEMLATE VARCHAR2(1000) := '\\\\dpc.tax.nalog.ru\\root\\GRs_UDEA\\gr216';
  MAX_ROWS_PROCESSED NUMBER(20, 0) := 100;
  begin
    update FNS_UD_CE_USER.DOCVERSION
    set U8518_URLPATH = REPLACE(U8518_URLPATH, SEARCH_TEMLATE, REPLACE_TEMLATE)
    where OBJECT_ID in (select OBJECT_ID from (select OBJECT_ID from FNS_UD_CE_USER.DOCVERSION
    where U8518_URLPATH like SEARCH_TEMLATE || '%' and
        OBJECT_CLASS_ID in (select OBJECT_ID from FNS_UD_CE_USER.CLASSDEFINITION
        where SYMBOLIC_NAME = 'TechInboxDocument')/*order by PUBLISHED_DATE desc*/)
    where ROWNUM <= MAX_ROWS_PROCESSED);
  end;
/