create procedure FNS_UD_EA_COMMON_USER.SPLOG(P_MESSAGE in VARCHAR2) as
  begin
    insert into FNS_UD_EA_COMMON_USER.SP_LOG_TABLE (DATATIME, MESSAGE) values (SYSDATE, P_MESSAGE);
    commit;
  end SPLOG;
/