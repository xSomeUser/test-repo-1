create procedure FNS_UD_EA_COMMON_USER.P$TARA_CONTROL(P_RECORD_NUMBER in VARCHAR2) as
  begin
    delete from FNS_UD_EA_COMMON_USER.KPI_TARA_CONTROL where TARA_RECORD_NUMBER = P_RECORD_NUMBER;
    insert into FNS_UD_EA_COMMON_USER.KPI_TARA_CONTROL (
      ID,
      ACT_RECORD_NUMBER,
      TARA_INVENTORY_NUMBER,
      TARA_RECORD_NUMBER,
      RECORD_DATE,
      DOC_BATCH_COUNT,
      DOC_COUNT,
      SHEET_COUNT,
      TASK_SENDING_DATE,
      PROTOCOL_RECEIVING_DATE,
      RCV_OPERATION_EXECUTING_DATE,
      TARA_STATUS,
      KNO,
      MODIFY_DATE
    )
      select
        FNS_UD_EA_COMMON_USER.HIBERNATE_SEQUENCE.NEXTVAL,
        UDDV2.U16D8_RECORDNUMBER,
        UDDV.UD1A8_TARAINVENTORYNUMBER,
        UDDV.U16D8_RECORDNUMBER,
        UDDV.U0FF3_RECORDDATE,
        UDDV.U15E6_DOCSETCOUNT as BATCH_COUNT,
        UDDV.U00B6_DOCCOUNT,
        UDDV.U7E76_SHEETCOUNT,
        UDDV2.U0FF3_RECORDDATE,
        U4.U0FF3_RECORDDATE,
        U3.U0FF3_RECORDDATE,
        UDDV.U5B08_STATUS,
        UDDV.UEA08_TAXAUTHORITYCODE,
        UDDV.MODIFY_DATE
      from FNS_UD_CE_USER.DOCVERSION UDDV
      left join (FNS_UD_CE_USER.LINK UDLINK
      join FNS_UD_CE_USER.DOCVERSION UDDV2 on UDLINK.HEAD_ID = UDDV2.OBJECT_ID and UDDV2.OBJECT_CLASS_ID = HEXTORAW('1477AD229B4FAA41B7BB3A34242D2401') --UD_ActHandover
        ) on UDLINK.TAIL_ID = UDDV.OBJECT_ID
      left join (
                  select
                    UDLINK3.HEAD_ID,
                    UDDV4.U0FF3_RECORDDATE,
                    rank() over (partition by HEAD_ID order by UDDV4.U0FF3_RECORDDATE desc) RNK
                  from FNS_UD_CE_USER.LINK UDLINK3
                  join FNS_UD_CE_USER.DOCVERSION UDDV4 on UDDV4.OBJECT_ID = UDLINK3.TAIL_ID and UDDV4.OBJECT_CLASS_ID = HEXTORAW('AF9CC896747CB64586037392675C9DC3') --UD_ProcessDoc
                ) U3 on U3.HEAD_ID = UDDV.OBJECT_ID and U3.RNK = 1
      left join (
                  select
                    UDLINK2.HEAD_ID,
                    UDDV3.U0FF3_RECORDDATE,
                    rank() over (partition by HEAD_ID order by UDDV3.U0FF3_RECORDDATE desc) RNK
                  from FNS_UD_CE_USER.LINK UDLINK2
                  join FNS_UD_CE_USER.DOCVERSION UDDV3 on UDLINK2.TAIL_ID = UDDV3.OBJECT_ID and UDDV3.OBJECT_CLASS_ID = HEXTORAW('2B2D23FB7428634FB11029391347BD44') --UD_ReceiptTaraProtocol
                ) U4 on U4.HEAD_ID = UDDV2.OBJECT_ID and U4.RNK = 1
      where UDDV.U16D8_RECORDNUMBER = P_RECORD_NUMBER;
  end;
/