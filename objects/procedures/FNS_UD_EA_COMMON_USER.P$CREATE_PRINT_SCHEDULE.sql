create procedure FNS_UD_EA_COMMON_USER.P$CREATE_PRINT_SCHEDULE(CPS_ in VARCHAR2, PRINTING_TYPE_ in NUMBER, START_DATE_ in DATE, END_DATE_ in DATE, FIO_ in VARCHAR2, PS_ID out VARCHAR2) as
  PS_ID_ RAW(16);
  CNT_ NUMBER(11, 0);
  CNT_SNU NUMBER(11, 0);
  CNT_TU NUMBER(11, 0);
  CNT_SNU_TU NUMBER(11, 0);
  cursor C_IR is
    select * from FNS_UD_EA_COMMON_USER.PO$INFORMATION_RESOURCE
    where STATUS = 'CREATED' and
        PARENT_RN is null and
        PRINTING_TYPE = PRINTING_TYPE_ and RECEIVER_CODE = CPS_
    order by ACTUAL_PRIORITY desc
    for update of PO$PRINT_SCHEDULE_ID, STATUS;
  IR FNS_UD_EA_COMMON_USER.PO$INFORMATION_RESOURCE%ROWTYPE;
  begin
    open C_IR;
    for PCC in ( select * from FNS_UD_EA_COMMON_USER.NSI$PRINT_CENTERS_CAPACITIES
    where RECEIVER_CODE = CPS_ and PRINTING_TYPE = PRINTING_TYPE_ and IS_ANNULLED = 0) loop
      insert into FNS_UD_EA_COMMON_USER.PO$PRINT_SCHEDULE
      (
        NSI$PCC_ID,
        START_DATE,
        END_DATE,
        FIO,
        COUNT_SNU,
        COUNT_TU,
        STATUS,
        DATE_STATUS,
        CREATE_DATE,
        MODIFY_DATE,
        LOCKED
      )
      values
        (
          PCC.ID,
          START_DATE_,
          END_DATE_,
          FIO_,
          0,
          0,
          'PREPARED',
          SYSDATE,
          SYSDATE,
          SYSDATE,
          0
        ) returning ID into ps_id_;
      CNT_SNU := 0;
      CNT_TU := 0;
      CNT_SNU_TU := 0;
      for CNT in 1..PCC.CAPACITY loop
        fetch C_IR into IR;
        exit when C_IR%notfound;
        update FNS_UD_EA_COMMON_USER.PO$INFORMATION_RESOURCE
        set PO$PRINT_SCHEDULE_ID = PS_ID_,
          STATUS = 'IN_SENDING_QUEUE'
        where current of C_IR;
        if (IR.DELIVERY_TYPE = 1) then -- ���� ������������ - �� ��������� ������ � ���������� � ����
          update FNS_UD_EA_COMMON_USER.PO$INFORMATION_RESOURCE
          set --PO$PRINT_SCHEDULE_ID = ps_id_,
            STATUS = 'IN_SENDING_QUEUE'
          where STATUS = 'CREATED' and PARENT_RN = IR.RECORD_NUMBER;
        end if;
        if (IR.DELIVERY_TYPE = 1) then
          CNT_SNU_TU := CNT_SNU_TU + 1;
        else
          if IR.NOTIFICATION_FORM = '01' then
            CNT_SNU := CNT_SNU + 1;
          else
            CNT_TU := CNT_TU + 1;
          end if;
        end if;
      end loop;
      update FNS_UD_EA_COMMON_USER.PO$PRINT_SCHEDULE
      set COUNT_SNU = CNT_SNU,
        COUNT_TU = CNT_TU,
        COUNT_SNU_TU = CNT_SNU_TU
      where ID = PS_ID_;
      exit when C_IR%notfound;
    end loop;
    close C_IR;
    PS_ID := RAWTOHEX(PS_ID_);
  end;
/