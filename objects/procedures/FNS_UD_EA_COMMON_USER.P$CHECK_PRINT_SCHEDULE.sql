create procedure FNS_UD_EA_COMMON_USER.P$CHECK_PRINT_SCHEDULE(CPS_ in VARCHAR2, PRINTING_TYPE_ in NUMBER, START_DATE_ in DATE, END_DATE_ in DATE, ALREADY_EXISTS out NUMBER, /*0 - ��� �����, 1 - ��� ���� ���� �� ���� ������ */  NEW_START_DATE out DATE /* ���� ��������� ������������� ����� +1 ���� */) as
  begin
    ALREADY_EXISTS := 0;
    NEW_START_DATE := null;
    select trunc(Max(PS.END_DATE)) + 1 into NEW_START_DATE
    from FNS_UD_EA_COMMON_USER.PO$PRINT_SCHEDULE PS
    inner join FNS_UD_EA_COMMON_USER.NSI$PRINT_CENTERS_CAPACITIES PCC on (PS.NSI$PCC_ID = PCC.ID)
    where
      PCC.IS_ANNULLED = 0 and
          PCC.RECEIVER_CODE = CPS_ and
          PCC.PRINTING_TYPE = PRINTING_TYPE_ and
          (
            TRUNC(PS.START_DATE) between TRUNC(START_DATE_) and TRUNC(END_DATE_) or
                TRUNC(PS.END_DATE) between TRUNC(START_DATE_) and TRUNC(END_DATE_) or
                (TRUNC(PS.START_DATE) < TRUNC(START_DATE_) and TRUNC(PS.END_DATE) > TRUNC(END_DATE_))
          );
    if NEW_START_DATE is not null then
      ALREADY_EXISTS := 1;
    end if;
  end;
/