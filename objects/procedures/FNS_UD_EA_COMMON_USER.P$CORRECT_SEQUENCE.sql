create procedure FNS_UD_EA_COMMON_USER.P$CORRECT_SEQUENCE(SEQUENCE_NAME in VARCHAR2, CURRENT_VALUE in NUMBER) as
  L_MAX_SEQ NUMBER;
  DIFF_VAL NUMBER;
  begin
    if SEQUENCE_NAME is null then return;
    end if;
    if CURRENT_VALUE is null then return;
    end if;
    SYS.DBMS_OUTPUT.PUT_LINE('SEQUENCE_NAME = ' || SEQUENCE_NAME || ', CURRENT_VALUE = ' || CURRENT_VALUE);
    -- Get the maximum ID
    --SELECT MAX(id) INTO l_max_id FROM tmp_test;
    -- Get the current sequence value;
    execute immediate 'select ' || SEQUENCE_NAME || '.nextval from dual' into L_MAX_SEQ;
    SYS.DBMS_OUTPUT.PUT_LINE('l_max_seq = ' || L_MAX_SEQ);
    --SELECT test_seq.currval
    --INTO l_max_seq
    --FROM dual;
    select (CURRENT_VALUE - L_MAX_SEQ) into DIFF_VAL from DUAL;
    if DIFF_VAL = 0 then
      SYS.DBMS_OUTPUT.PUT_LINE('�������� �������� ��������� � ��� � �������� ��� ���� ��������.');
      return;
    end if;
    -- Alter the sequence to increment by the difference ( -5 in this case ).
    execute immediate 'alter sequence ' || SEQUENCE_NAME || ' increment by ' || DIFF_VAL || ' minvalue 0';
    -- 'increment' by -5
    execute immediate 'SELECT ' || SEQUENCE_NAME || '.nextval FROM dual' into L_MAX_SEQ;
    -- Change the sequence back to normal
    execute immediate 'alter sequence ' || SEQUENCE_NAME || ' increment by 1';
    SYS.DBMS_OUTPUT.PUT_LINE('l_max_seq = ' || L_MAX_SEQ);
  end P$CORRECT_SEQUENCE;
/