create procedure FNS_UD_EA_COMMON_USER.P$ADD_RECORD_TO_KPI_EVENTING(P_AREA_NAME in VARCHAR2 := 'DEFAULTAREA', P_DOMAIN_NAME in VARCHAR2 := 'ElectronicArchiveService', P_EVENT_TYPE_NAME in VARCHAR2 := 'TaxOutcomingNotDelivered', P_DATA_KEY in VARCHAR2) as
  V_AREA_ID NUMBER;
  V_EVENT_TYPE_KEY NUMBER;
  begin
    V_AREA_ID := I$CSC_PF.F$GET_AREA_ID(P_AREA_NAME);
    V_EVENT_TYPE_KEY := I$CSC_PF.PAC$INT_EVENTING.F$GET_EVENT_TYPE_KEY(V_AREA_ID, P_DOMAIN_NAME, P_EVENT_TYPE_NAME);
    I$CSC_PF.PAC$INT_EVENTING.P$PUBLISH(V_AREA_ID, V_EVENT_TYPE_KEY, P_DATA_KEY);
  end;
/
create procedure FNS_UD_EA_COMMON_USER.P$ALL_TASKS(P_RECORD_NUMBER in VARCHAR2) as
  C_STATUS_CREATED FNS_EA_CE_USER.DOCVERSION.U5B08_STATUS%TYPE := 'CREATED';
  C_STATUS_SENT FNS_EA_CE_USER.DOCVERSION.U5B08_STATUS%TYPE := 'SENT';
  C_STATUS_PROCESSED FNS_EA_CE_USER.DOCVERSION.U5B08_STATUS%TYPE := 'PROCESSED';
  C_STATUS_PROCESSED_UNSUCCESS FNS_EA_CE_USER.DOCVERSION.U5B08_STATUS%TYPE := 'PROCESSED_UNSUCCESSFULLY';
  C_STATUS_ON_STORAGE FNS_EA_CE_USER.DOCVERSION.U5B08_STATUS%TYPE := 'ON_STORAGE';
  C_STATUS_PROCESSING FNS_EA_CE_USER.DOCVERSION.U5B08_STATUS%TYPE := 'PROCESSING';
  C_STATUS_CANCELED FNS_EA_CE_USER.DOCVERSION.U5B08_STATUS%TYPE := 'CANCELED';
  C_CLASSNAME_EA_RD FNS_EA_CE_USER.CLASSDEFINITION.SYMBOLIC_NAME%TYPE := 'EA_RD';
  C_CLASSNAME_EA_RDSET FNS_EA_CE_USER.CLASSDEFINITION.SYMBOLIC_NAME%TYPE := 'EA_RDSet';
  C_CLASSNAME_EA_TAXINCOMING FNS_EA_CE_USER.CLASSDEFINITION.SYMBOLIC_NAME%TYPE := 'EA_TaxIncoming';
  C_CLASSNAME_UD_PROCESSDOC FNS_EA_CE_USER.CLASSDEFINITION.SYMBOLIC_NAME%TYPE := 'UD_ProcessDoc';
  C_CLASSNAME_UD_SUPPLIEDSHEET FNS_EA_CE_USER.CLASSDEFINITION.SYMBOLIC_NAME%TYPE := 'UD_SuppliedSheet';
  C_CLASSNAME_UD_ACTHANDOVER FNS_EA_CE_USER.CLASSDEFINITION.SYMBOLIC_NAME%TYPE := 'UD_ActHandover';
  C_FNS_EA_LC_SCANANDDIGITIZE FNS_EA_CE_USER.DOCVERSION.UE988_ANNOTATIONPROCESSNAME%TYPE := 'fns-ea-lc-scanAndDigitize';
  C_FNS_EA_LC_SCAN FNS_EA_CE_USER.DOCVERSION.UE988_ANNOTATIONPROCESSNAME%TYPE := 'fns-ea-lc-scan';
  C_FNS_EA_LC_NOSCANANDDIGITIZE FNS_EA_CE_USER.DOCVERSION.UE988_ANNOTATIONPROCESSNAME%TYPE := 'fns-ea-lc-NOScanAndDigitize';
  C_FNS_EA_LC_DIGITIZE FNS_EA_CE_USER.DOCVERSION.UE988_ANNOTATIONPROCESSNAME%TYPE := 'fns-ea-lc-digitize';
  C_FNS_EA_LC_STORE FNS_EA_CE_USER.DOCVERSION.UE988_ANNOTATIONPROCESSNAME%TYPE := 'fns-ea-lc-store';
  C_FNS_EA_LC_SELECTIVESCAN FNS_EA_CE_USER.DOCVERSION.UE988_ANNOTATIONPROCESSNAME%TYPE := 'fns-ea-lc-selectiveScan';
  C_FNS_EA_LC_ISSUANCEOFTEMP_ FNS_EA_CE_USER.DOCVERSION.UE988_ANNOTATIONPROCESSNAME%TYPE := 'fns-ea-lc-issuanceOfTemporaryUse';
  begin
    delete from FNS_UD_EA_COMMON_USER.KPI_ALL_TASKS where RECORD_NUMBER = P_RECORD_NUMBER;
    insert into FNS_UD_EA_COMMON_USER.KPI_ALL_TASKS (
      ID,
      DOC_KIND,
      RECORD_NUMBER,
      RECORD_DATE,
      DOC_TITLE,
      DESCRIPTION_TASK,
      REGISTRATION_NUMBER,
      STATUS,
      PROCESS_NAME,
      STATUS_DATE,
      KNO,
      INVENTORY_NUMBER,
      CREATE_DATE,
      RECEIVE_DATE
    )
      select
        FNS_UD_EA_COMMON_USER.HIBERNATE_SEQUENCE.NEXTVAL,
        DOC_CATEGORY,
        RECORD_NUMBER,
        RECORD_DATE,
        DOC_TITLE,
        TASK,
        REG_NUMBER,
        STATUS,
        (case
         when STATUS = (select distinct TRANSLATED from FNS_UD_EA_COMMON_USER.CUR_TASKS_STATES where ID = 10)
             and PROCESS_NAME not in (select TRANSLATED from FNS_UD_EA_COMMON_USER.PR_NAME_TRANSLATIONS where ORIGINAL in (C_FNS_EA_LC_ISSUANCEOFTEMP_, C_FNS_EA_LC_SELECTIVESCAN))
           then ''
         else PROCESS_NAME
         end) as PROCESS_NAME,
        MODIFY_DATE,
        KNO,
        TARA_INVENTORY_NUMBER,
        SYSTIMESTAMP,
        RECEIVE_DATE
      from (
        select distinct
          UDEA_DOCTYPE_TRANSLATIONS.TRANSLATED as DOC_CATEGORY,
          EADV.U16D8_RECORDNUMBER as RECORD_NUMBER,
          EADV.U0FF3_RECORDDATE as RECORD_DATE,
          EADV.U7B18_DOCTITLE as DOC_TITLE,
          UDEA_DESCRIPTIONTASKTRANS.TRANSLATED as TASK,
          (case
           when EACD.SYMBOLIC_NAME = C_CLASSNAME_EA_RD
             then
               (
                 select U9A38_REGISTRATIONNUMBER from FNS_EA_CE_USER.DOCVERSION EADV2
                 join FNS_EA_CE_USER.CLASSDEFINITION EACD2 on EADV2.OBJECT_CLASS_ID = EACD2.OBJECT_ID and EACD2.SYMBOLIC_NAME = C_CLASSNAME_EA_RDSET
                 join FNS_EA_CE_USER.LINK EALINK on EADV2.OBJECT_ID = EALINK.HEAD_ID
                 where EADV.OBJECT_ID = EALINK.TAIL_ID
               )
           when EACD.SYMBOLIC_NAME = C_CLASSNAME_EA_TAXINCOMING
             then EADV.U9A38_REGISTRATIONNUMBER
           end) as REG_NUMBER,
          (select TRANSLATED from FNS_UD_EA_COMMON_USER.CUR_TASKS_STATES
          where ID =
              (case
               when EADV.U5B08_STATUS = C_STATUS_CREATED
                 then
                   case
                   when EAVW."RECORDNUMBER" is null
                     then 1
                   when
                     (select count(*) from FNS_UD_CE_USER.DOCVERSION UDDV
                     join FNS_UD_CE_USER.CLASSDEFINITION UDCD on UDDV.OBJECT_CLASS_ID = UDCD.OBJECT_ID and UDCD.SYMBOLIC_NAME = C_CLASSNAME_UD_PROCESSDOC
                     join FNS_UD_CE_USER.LINK UDLINK on UDDV.OBJECT_ID = UDLINK.TAIL_ID
                     join FNS_UD_CE_USER.DOCVERSION UDDV2 on UDLINK.HEAD_ID = UDDV2.OBJECT_ID
                     join FNS_UD_CE_USER.CLASSDEFINITION UDCD2 on UDDV2.OBJECT_CLASS_ID = UDCD2.OBJECT_ID and UDCD2.SYMBOLIC_NAME = C_CLASSNAME_UD_SUPPLIEDSHEET
                     where UDDV.U24A8_EANUMBER = EADV.U16D8_RECORDNUMBER
                     ) = 1
                     then 2
                   when
                     (select count(*) from FNS_UD_CE_USER.DOCVERSION UDDV
                     join FNS_UD_CE_USER.CLASSDEFINITION UDCD on UDDV.OBJECT_CLASS_ID = UDCD.OBJECT_ID and UDCD.SYMBOLIC_NAME = C_CLASSNAME_UD_PROCESSDOC
                     join FNS_UD_CE_USER.LINK UDLINK on UDDV.OBJECT_ID = UDLINK.TAIL_ID
                     join FNS_UD_CE_USER.DOCVERSION UDDV2 on UDLINK.HEAD_ID = UDDV2.OBJECT_ID
                     join FNS_UD_CE_USER.CLASSDEFINITION UDCD2 on UDDV2.OBJECT_CLASS_ID = UDCD2.OBJECT_ID and UDCD2.SYMBOLIC_NAME = C_CLASSNAME_UD_SUPPLIEDSHEET
                     join FNS_UD_CE_USER.LINK UDLINK2 on UDDV2.OBJECT_ID = UDLINK2.TAIL_ID
                     join FNS_UD_CE_USER.DOCVERSION UDDV3 on UDLINK2.HEAD_ID = UDDV3.OBJECT_ID and UDDV3.U5B08_STATUS = C_STATUS_SENT
                     join FNS_UD_CE_USER.CLASSDEFINITION UDCD3 on UDDV3.OBJECT_CLASS_ID = UDCD3.OBJECT_ID and UDCD3.SYMBOLIC_NAME = C_CLASSNAME_UD_ACTHANDOVER
                     where UDDV.U24A8_EANUMBER = EADV.U16D8_RECORDNUMBER
                     ) = 1
                     then 3
                   end
               when EADV.U5B08_STATUS = C_STATUS_PROCESSED
                   and EADV.UE988_ANNOTATIONPROCESSNAME in (C_FNS_EA_LC_SCANANDDIGITIZE, C_FNS_EA_LC_SCAN)
                 then 4
               when EADV.U5B08_STATUS = C_STATUS_PROCESSED_UNSUCCESS
                 then 5
               when EADV.U5B08_STATUS = C_STATUS_PROCESSED
                 then
                   case
                   when EADV.UE988_ANNOTATIONPROCESSNAME = C_FNS_EA_LC_NOSCANANDDIGITIZE
                     then 6
                   when EADV.UE988_ANNOTATIONPROCESSNAME = C_FNS_EA_LC_DIGITIZE
                     then 7
                   when EADV.U8906_PAPERISSUESTATUS = 1 and EADV.UE988_ANNOTATIONPROCESSNAME = C_FNS_EA_LC_STORE
                     then 8
                   when EADV.U8906_PAPERISSUESTATUS = 2 and EADV.UE988_ANNOTATIONPROCESSNAME = C_FNS_EA_LC_ISSUANCEOFTEMP_
                     then 9
                   end
               when EADV.U5B08_STATUS = C_STATUS_ON_STORAGE
                 then 10
               when EADV.U5B08_STATUS in (C_STATUS_ON_STORAGE, C_STATUS_PROCESSED) and EADV.UE988_ANNOTATIONPROCESSNAME = C_FNS_EA_LC_SELECTIVESCAN
                 then 11
               when EADV.U5B08_STATUS = C_STATUS_PROCESSING
                 then 12
               when EADV.U5B08_STATUS = C_STATUS_CREATED
                 then 13
               when EADV.U5B08_STATUS = C_STATUS_CANCELED
                 then 18
               end
              )
          ) as STATUS,
          UDEA_PROCESSNAMETRANS.TRANSLATED as PROCESS_NAME,
          EADV.MODIFY_DATE as MODIFY_DATE,
          EADV.UEA08_TAXAUTHORITYCODE as KNO,
          (select UDDV2.UD1A8_TARAINVENTORYNUMBER from FNS_UD_CE_USER.DOCVERSION UDDV
          join FNS_UD_CE_USER.CLASSDEFINITION UDCD on UDDV.OBJECT_CLASS_ID = UDCD.OBJECT_ID and UDCD.SYMBOLIC_NAME = C_CLASSNAME_UD_PROCESSDOC
          join FNS_UD_CE_USER.LINK UDLINK on UDDV.OBJECT_ID = UDLINK.TAIL_ID
          join FNS_UD_CE_USER.DOCVERSION UDDV2 on UDLINK.HEAD_ID = UDDV2.OBJECT_ID
          join FNS_UD_CE_USER.CLASSDEFINITION UDCD2 on UDDV2.OBJECT_CLASS_ID = UDCD2.OBJECT_ID and UDCD2.SYMBOLIC_NAME = C_CLASSNAME_UD_SUPPLIEDSHEET
          where UDDV.U24A8_EANUMBER = EADV.U16D8_RECORDNUMBER
          ) as TARA_INVENTORY_NUMBER,
          EADV.U7C43_RECEIVEDATE as RECEIVE_DATE
        from FNS_EA_CE_USER.DOCVERSION EADV
        join FNS_EA_CE_USER.CLASSDEFINITION EACD on EADV.OBJECT_CLASS_ID = EACD.OBJECT_ID
        join FNS_UD_EA_COMMON_USER.DOCTYPE_TRANSLATIONS UDEA_DOCTYPE_TRANSLATIONS on EACD.SYMBOLIC_NAME = UDEA_DOCTYPE_TRANSLATIONS.ORIGINAL
        join FNS_UD_EA_COMMON_USER.DESCR_TASK_TRANSLATIONS UDEA_DESCRIPTIONTASKTRANS on EADV.U86B8_DESCRIPTIONTASK = UDEA_DESCRIPTIONTASKTRANS.ORIGINAL
        left join FNS_SYS_USER.VWVR1_DEFAULTROSTER EAVW on EADV.U16D8_RECORDNUMBER = EAVW."RECORDNUMBER"
        left join FNS_UD_EA_COMMON_USER.PR_NAME_TRANSLATIONS UDEA_PROCESSNAMETRANS on EAVW."F_CLASS" = UDEA_PROCESSNAMETRANS.ORIGINAL
        where EADV.U16D8_RECORDNUMBER = P_RECORD_NUMBER
      );
    commit;
  end;
/