create procedure FNS_UD_EA_COMMON_USER.CLEAN_TABLES as
  C_FTE_TIMEOUT NUMBER := 5;
  /* 5 days */
  C_KPI_TIMEOUT NUMBER := 30;
  /* 30 days */
  C_SYSDATE DATE := SYSDATE;
  type T_PING_FILENAME is TABLE OF FNS_UD_EA_COMMON_USER.FTE_PING_ITEM.FILE_NAME%TYPE index by pls_integer;
  V_PING_FILENAME T_PING_FILENAME;
  begin
    delete from FNS_UD_EA_COMMON_USER.FTE_PONG_ITEM where RECEIVED_TIME <= C_SYSDATE - C_FTE_TIMEOUT;
    select FILE_NAME bulk collect into V_PING_FILENAME from FNS_UD_EA_COMMON_USER.FTE_PING_ITEM where SENT_TIME <= C_SYSDATE - C_FTE_TIMEOUT;
    delete from FNS_UD_EA_COMMON_USER.FTE_PING_ITEM where SENT_TIME <= C_SYSDATE - C_FTE_TIMEOUT;
    forall CNT in V_PING_FILENAME.FIRST..V_PING_FILENAME.LAST
    delete from FNS_UD_EA_COMMON_USER.FTE_PONG_ITEM where PING_FILE_NAME = v_ping_filename(CNT);
    delete from FNS_UD_EA_COMMON_USER.KPI_CURRENT_TASKS where MODIFY_DATE <= C_SYSDATE - C_KPI_TIMEOUT;
    delete from FNS_UD_EA_COMMON_USER.KPI_ALL_TASKS where STATUS_DATE <= C_SYSDATE - C_KPI_TIMEOUT;
    delete from FNS_UD_EA_COMMON_USER.KPI_TARA_CONTROL where MODIFY_DATE <= C_SYSDATE - C_KPI_TIMEOUT;
    delete from FNS_UD_EA_COMMON_USER.PROCESS_STATUS T1 where T1.ERROR = 0 and T1.TIME_STAMP < C_SYSDATE - C_KPI_TIMEOUT
        and exists(select 1 from FNS_UD_EA_COMMON_USER.PROCESS_STATUS T2 where T2.THREAD_ID = T1.THREAD_ID
        and (T2.STEP = 'start' and T1.STEP = 'end' or T1.STEP = 'start' and T2.STEP = 'end')); -- ��� t1.step <> t2.step
    --	commit;
    exception
    when others then
    null;
  end;
/