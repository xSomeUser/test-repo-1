create procedure FNS_UD_EA_COMMON_USER.P$PO$SET_PREFERABLE_DATES(IR_ID VARCHAR2, ST_DATE DATE, END_DATE DATE) as
  SD DATE;
  ED DATE;
  begin
    if TRUNC(ST_DATE) = TO_DATE('29.06.1972', 'DD.MM.YYYY') then
      SD := null;
    else
      SD := ST_DATE;
    end if;
    if TRUNC(END_DATE) = TO_DATE('29.06.1972', 'DD.MM.YYYY') then
      ED := null;
    else
      ED := END_DATE;
    end if;
    update FNS_UD_EA_COMMON_USER.PO$INFORMATION_RESOURCE
    set PREFERABLEPRSTARTDATE = SD,
      PREFERABLEPRENDDATE = ED
    where ID = HEXTORAW(IR_ID) and STATUS = 'CREATED';
  end;
/