create procedure FNS_UD_EA_COMMON_USER.CREATE_IB_RESOURCE(DOC_RN in VARCHAR2, FILE_NAME in VARCHAR2, FILE_PATH in VARCHAR2, HASH_CODE in VARCHAR2, UUID in VARCHAR2) as
  IB_ID NUMBER;
  BR_ID PLS_INTEGER;
  begin
    -- get id by rn
    select IB.ID into IB_ID
    from "FNS_UD_EA_COMMON_USER"."INCOMING_BUFFER" IB where IB.RECORD_NUMBER = DOC_RN and ROWNUM = 1;
    -- ��������� ������ ����������� ���������� �����
    BR_ID := HIBERNATE_SEQUENCE.NEXTVAL;
    insert into "FNS_UD_EA_COMMON_USER"."BUFFER_RESOURCE" (
      "ID",
      "FILE_NAME",
      "FILE_PATH",
      "HASH_CODE",
      "RECORD_NUMBER",
      "STATUS",
      "UUID"
    )
    values (
      BR_ID,
      FILE_NAME, -- ������ ��� �����
      FILE_PATH, -- ������ ���� � �����
      HASH_CODE, -- MD5 ��� ����� - ����������� ����� �����
      DOC_RN, -- ������� ����� ��������� � ���. ��������� �������� ������
      'CREATED', -- ��������� ������ ��������� ������� CREATED
      UUID -- ������� ����� ��������� � ���. ��������� �������� ������
    );
    insert into "FNS_UD_EA_COMMON_USER"."IB_RESOURCE" (
      "INCOMING_BUFFER_ID",
      "BUFFERED_RESOURCE_ID"
    ) values (
      IB_ID,
      BR_ID
    );
  end CREATE_IB_RESOURCE;
/