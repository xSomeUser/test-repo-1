create procedure FNS_UD_EA_COMMON_USER.P$CREATE_INDEX_IF_NOT_EXIST(IDX_OWNER VARCHAR2, IDX_NAME VARCHAR2, IDX_TABLE_FIELDS VARCHAR2) as
  CNT NUMBER;
  begin
    select COUNT(*) into CNT
    from ALL_INDEXES where OWNER = IDX_OWNER and INDEX_NAME = IDX_NAME;
    if CNT = 0 then
      DBMS_OUTPUT.PUT_LINE('sql: create index ' || IDX_OWNER || '.' || IDX_NAME || ' on ' || IDX_OWNER || '.' || IDX_TABLE_FIELDS || ' online parallel 32');
      execute immediate 'create index ' || IDX_OWNER || '.' || IDX_NAME || ' on ' || IDX_OWNER || '.' || IDX_TABLE_FIELDS || ' online parallel 32';
      DBMS_OUTPUT.PUT_LINE('sql: alter index ' || IDX_OWNER || '.' || IDX_NAME || ' noparallel');
      execute immediate 'alter index ' || IDX_OWNER || '.' || IDX_NAME || ' noparallel';
    end if;
  end;
/