create procedure FNS_UD_EA_COMMON_USER.P$COPYINDEXFROMTEMP(PART_NAME in VARCHAR2) as
  FOLDERID NUMBER;
  FILEID NUMBER;
  COUNT_CHECK NUMBER;
  TABLE_NAME VARCHAR2(256);
  begin
    select UPPER(PART_NAME) into TABLE_NAME from DUAL;
    if TABLE_NAME = 'FOLDER' then --������������ �������� � �������
      --    SYS.DBMS_OUTPUT.PUT_LINE('folder: Part_Name ="'||Part_Name||'"');
      --�������� ��������� ID ��� ����� �� ��������
      select CONVINDFOLDSEQ.NEXTVAL - 1 into FOLDERID from DUAL;
      merge into CONV_ID_TEMP CONV_ID using DUAL on (CONV_ID.TABLENAME = TABLE_NAME)
      when not matched then
      insert (TABLENAME, STARTINDEX) values (TABLE_NAME, FOLDERID)
      when matched then
      update set STARTINDEX = FOLDERID;
      execute immediate 'INSERT INTO CONV_INDEXFOLDERS (ID,INDEXID,FOLDERNAME) SELECT ID+' || FOLDERID || ',indexid,foldername FROM CONV_INDEXFOLDERS_TEMP';
      SYS.DBMS_OUTPUT.PUT_LINE('��������� ����� � ������� CONV_INDEXFOLDERS=' || sql%rowcount);
      select MAX(ID) into FOLDERID from CONV_INDEXFOLDERS;
      P$CORRECT_SEQUENCE('CONVINDFOLDSEQ', FOLDERID);
      merge into CONV_ID_TEMP CONV_ID using DUAL on (CONV_ID.TABLENAME = TABLE_NAME)
      when not matched then
      insert (TABLENAME, ENDINDEX) values (TABLE_NAME, FOLDERID)
      when matched then
      update set ENDINDEX = FOLDERID;
    elsif TABLE_NAME = 'FILE' then --������������ �������� � �������
      --    SYS.DBMS_OUTPUT.PUT_LINE('files: Part_Name ="'||Part_Name||'"');
      --����������� ��������� ID ����� �� ������� CONV_ID_TEMP
      select count(1) into COUNT_CHECK from CONV_ID_TEMP where TABLENAME = 'FOLDER';
      if COUNT_CHECK = 0 then RAISE_APPLICATION_ERROR(-20100, '�� ����������� ����������� ������ �� ������� CONV_INDEXFOLDERS.
                            ���������� ������ �� ������� CONV_INDEXFOLDERS, � ����� ��������� ����������� �� ����� CONV_INDEXFILES');
      end if;
      select STARTINDEX into FOLDERID from CONV_ID_TEMP where TABLENAME = 'FOLDER';
      --�������� ��������� ID ��� ������ �� ��������
      select CONVINDFILESEQ.NEXTVAL - 1 into FILEID from DUAL;
      --��������� ID �����
      merge into CONV_ID_TEMP CONV_ID using DUAL on (CONV_ID.TABLENAME = TABLE_NAME)
      when not matched then
      insert (TABLENAME, STARTINDEX) values (TABLE_NAME, FILEID)
      when matched then
      update set STARTINDEX = FILEID;
      execute immediate 'INSERT INTO CONV_INDEXFILES (ID,FOLDERID,FILENAME,FILEEXT,INVALID,DOCID) select ID+' || FILEID || ',FOLDERID+' || FOLDERID || ',FILENAME,FILEEXT,INVALID,DOCID from CONV_INDEXFILES_TEMP';
      SYS.DBMS_OUTPUT.PUT_LINE('��������� ����� � ������� CONV_INDEXFILES=' || sql%rowcount);
      select MAX(ID) into FILEID from CONV_INDEXFILES;
      P$CORRECT_SEQUENCE('CONVINDFILESEQ', FILEID);
      merge into CONV_ID_TEMP CONV_ID using DUAL on (CONV_ID.TABLENAME = TABLE_NAME)
      when not matched then
      insert (TABLENAME, ENDINDEX) values (TABLE_NAME, FILEID)
      when matched then
      update set ENDINDEX = FILEID;
    else
      RAISE_APPLICATION_ERROR(-20200, '���������� ��� ������. Part_Name ="' || TABLE_NAME || '".
                            ���������� �������� "FOLDER","FILE"');
    end if;
    --  SELECT CONVINDFOLDSEQ.Nextval-1 INTO folderId FROM dual;
    --  SELECT CONVINDFILESEQ.Nextval-1 INTO fileId FROM dual;
    --  SYS.DBMS_OUTPUT.PUT_LINE('folderId='||folderId||', fileId='||fileId);
    --  EXECUTE IMMEDIATE 'INSERT INTO CONV_INDEXFOLDERS (ID,INDEXID,FOLDERNAME) SELECT ID+'||folderId||',indexid,foldername FROM CONV_INDEXFOLDERS_TEMP';
    --  SYS.DBMS_OUTPUT.PUT_LINE('��������� ����� � ������� CONV_INDEXFOLDERS='||sql%rowcount);
    --  EXECUTE IMMEDIATE 'INSERT INTO CONV_INDEXFILES (ID,FOLDERID,FILENAME,FILEEXT,INVALID,DOCID) select ID+'||fileId||',FOLDERID+'||folderId||',FILENAME,FILEEXT,INVALID,DOCID from CONV_INDEXFILES_TEMP';
    --  SYS.DBMS_OUTPUT.PUT_LINE('��������� ����� � ������� CONV_INDEXFILES='||sql%rowcount);
    --  SELECT MAX(id) INTO folderId FROM CONV_INDEXFOLDERS;
    --  SELECT MAX(id) INTO fileId FROM CONV_INDEXFILES;
    --  SYS.DBMS_OUTPUT.PUT_LINE('folderId='||folderId||', fileId='||fileId);
    --  P$CORRECT_SEQUENCE( 'CONVINDFOLDSEQ', folderId );
    --  P$CORRECT_SEQUENCE( 'CONVINDFILESEQ', fileId );
  end P$COPYINDEXFROMTEMP;
/