create procedure FNS_UD_EA_COMMON_USER.P$ADD_PRINT_CENTERS_CAPACITIES(CPS VARCHAR2, PRINT_TYPE NUMBER, FIO VARCHAR2, CAPACITY NUMBER) as
  NEW_ID RAW(16);
  OLD_CAP_ NUMBER;
  begin
    insert into FNS_UD_EA_COMMON_USER.NSI$PRINT_CENTERS_CAPACITIES
    (RECEIVER_CODE, PRINTING_TYPE, FIO, CAPACITY)
    values (P$ADD_PRINT_CENTERS_CAPACITIES.CPS, P$ADD_PRINT_CENTERS_CAPACITIES.PRINT_TYPE, P$ADD_PRINT_CENTERS_CAPACITIES.FIO, P$ADD_PRINT_CENTERS_CAPACITIES.CAPACITY)
    returning ID into new_id;
    update FNS_UD_EA_COMMON_USER.NSI$PRINT_CENTERS_CAPACITIES
    set IS_ANNULLED = 1
    where RECEIVER_CODE = P$ADD_PRINT_CENTERS_CAPACITIES.CPS and PRINTING_TYPE = P$ADD_PRINT_CENTERS_CAPACITIES.PRINT_TYPE and IS_ANNULLED = 0 and ID <> NEW_ID
    returning CAPACITY into old_cap_;
    update FNS_UD_EA_COMMON_USER.PO$PRINT_SCHEDULE
    set NSI$PCC_ID = NEW_ID
    where
      STATUS in ('CREATED', 'PREPARED') and
          NSI$PCC_ID in (select ID from FNS_UD_EA_COMMON_USER.NSI$PRINT_CENTERS_CAPACITIES
          where RECEIVER_CODE = P$ADD_PRINT_CENTERS_CAPACITIES.CPS and PRINTING_TYPE = P$ADD_PRINT_CENTERS_CAPACITIES.PRINT_TYPE and IS_ANNULLED = 1 and ID <> NEW_ID
          );
    /*
    if ( P$ADD_PRINT_CENTERS_CAPACITIES.CAPACITY < old_cap_) then
      -- ������� ������ ��
      FOR sched IN ( select * from FNS_UD_EA_COMMON_USER.PO$PRINT_SCHEDULE where NSI$PCC_ID = new_id and STATUS = 'CREATED' ORDER BY START_DATE asc)
      LOOP
        update FNS_UD_EA_COMMON_USER.PO$INFORMATION_RESOURCE
            set PO$PRINT_SCHEDULE_ID = null,
                STATUS = 'CREATED'
                where Id in (select Id from ( select Id from FNS_UD_EA_COMMON_USER.PO$INFORMATION_RESOURCE
                                where STATUS = 'IN_SENDING_QUEUE' and
                                      PO$PRINT_SCHEDULE_ID = sched.ID
                                      ORDER BY MANUAL_EDIT ASC, NVL(ACTUAL_PRIORITY,0) ASC) where ROWNUM <= old_cap_ - P$ADD_PRINT_CENTERS_CAPACITIES.CAPACITY);
        if SQL%ROWCOUNT > 0 then
             UPDATE FNS_UD_EA_COMMON_USER.PO$PRINT_SCHEDULE
              SET COUNT_SNU = (select count(*) from FNS_UD_EA_COMMON_USER.PO$INFORMATION_RESOURCE where PO$PRINT_SCHEDULE_ID = sched.ID and NOTIFICATION_FORM = '01'),
                  COUNT_TU = (select count(*) from FNS_UD_EA_COMMON_USER.PO$INFORMATION_RESOURCE where PO$PRINT_SCHEDULE_ID = sched.ID and NOTIFICATION_FORM = '02'),
                  COUNT_SNU_TU = (select count(*) from FNS_UD_EA_COMMON_USER.PO$INFORMATION_RESOURCE where PO$PRINT_SCHEDULE_ID = sched.ID and DELIVERY_TYPE = 1)
                  WHERE ID = sched.ID;
        end if;
      END LOOP;
    else
      -- �������� �������� ���������� ��
      FOR sched IN ( select * from FNS_UD_EA_COMMON_USER.PO$PRINT_SCHEDULE where NSI$PCC_ID = new_id and STATUS = 'CREATED'  ORDER BY START_DATE asc)
      LOOP
         update FNS_UD_EA_COMMON_USER.PO$INFORMATION_RESOURCE
            set PO$PRINT_SCHEDULE_ID = sched.ID,
                STATUS = 'IN_SENDING_QUEUE'
                where Id in (select Id from ( select Id from FNS_UD_EA_COMMON_USER.PO$INFORMATION_RESOURCE
                                where STATUS = 'CREATED' and
                                      PRINTING_TYPE = P$ADD_PRINT_CENTERS_CAPACITIES.PRINT_TYPE and
                                      PARENT_RN is null and
                                      RECEIVER_CODE = P$ADD_PRINT_CENTERS_CAPACITIES.CPS
                                      ORDER BY NVL(ACTUAL_PRIORITY,0) DESC) where ROWNUM <= P$ADD_PRINT_CENTERS_CAPACITIES.CAPACITY - old_cap_);
        if SQL%ROWCOUNT > 0 then
             UPDATE FNS_UD_EA_COMMON_USER.PO$PRINT_SCHEDULE
              SET COUNT_SNU = (select count(*) from FNS_UD_EA_COMMON_USER.PO$INFORMATION_RESOURCE where PO$PRINT_SCHEDULE_ID = sched.ID and NOTIFICATION_FORM = '01'),
                  COUNT_TU = (select count(*) from FNS_UD_EA_COMMON_USER.PO$INFORMATION_RESOURCE where PO$PRINT_SCHEDULE_ID = sched.ID and NOTIFICATION_FORM = '02'),
                  COUNT_SNU_TU = (select count(*) from FNS_UD_EA_COMMON_USER.PO$INFORMATION_RESOURCE where PO$PRINT_SCHEDULE_ID = sched.ID and DELIVERY_TYPE = 1)
                  WHERE ID = sched.ID;
        end if;
      END LOOP;
    end if;
    */
  end;
/