create procedure FNS_UD_EA_COMMON_USER.P$COPYCONVFROMTEMP(PART_NAME in VARCHAR2) as
  CONVFILESID NUMBER;
  CONVRDID NUMBER;
  CONVRKID NUMBER;
  CONVRDOCID NUMBER;
  COUNT_CHECK NUMBER;
  TABLE_NAME VARCHAR2(256);
  MAX_ID NUMBER;
  MIN_ID NUMBER;
  STEP_NUM NUMBER := 10000;
  PREV_ID NUMBER;
  begin
    select UPPER(PART_NAME) into TABLE_NAME from DUAL;
    if TABLE_NAME = 'CONVFILE' then --������������ �������� � ����������������� �������
      --    SYS.DBMS_OUTPUT.PUT_LINE('folder: Part_Name ="'||Part_Name||'"');
      --�������� ��������� ID ��� ���������������� ������ �� ��������
      select CONVFILESSEQ.NEXTVAL - 1 into CONVFILESID from DUAL;
      merge into CONV_ID_TEMP CONV_ID using DUAL on (CONV_ID.TABLENAME = TABLE_NAME)
      when not matched then
      insert (TABLENAME, STARTINDEX) values (TABLE_NAME, CONVFILESID)
      when matched then
      update set STARTINDEX = CONVFILESID;
      P$DISABLECONSTRAINT('CONV_FILES'); -- ���������� ����������� � ��������
      --�������� � ������� CONV_FILES
      execute immediate 'INSERT /*+ APPEND*/ INTO CONV_FILES (ID, RUNID, FILEPATH) SELECT /*+ PARALLEL(8)*/ ID+' || CONVFILESID || ',RUNID, FILEPATH FROM CONV_FILES_TEMP';
      SYS.DBMS_OUTPUT.PUT_LINE('��������� ����� � ������� CONV_FILES=' || sql%rowcount);
      commit;
      select max(ID) into CONVFILESID from CONV_FILES;
      P$CORRECT_SEQUENCE('CONVFILESSEQ', CONVFILESID);
      P$ENABLECONSTRAINT('CONV_FILES'); -- ��������� ����������� � ��������
      merge into CONV_ID_TEMP CONV_ID using DUAL on (CONV_ID.TABLENAME = TABLE_NAME)
      when not matched then
      insert (TABLENAME, ENDINDEX) values (TABLE_NAME, CONVFILESID)
      when matched then
      update set ENDINDEX = CONVFILESID;
    elsif TABLE_NAME = 'CONVRD' then --������������ �������� � ����������� ��
      --    SYS.DBMS_OUTPUT.PUT_LINE('files: Part_Name ="'||Part_Name||'"');
      --����������� ��������� ������ ���������������� ������ �� ������� CONV_ID_TEMP
      select count(1) into COUNT_CHECK from CONV_ID_TEMP where TABLENAME = 'CONVFILE';
      if COUNT_CHECK = 0 then RAISE_APPLICATION_ERROR(-20100, '�� ����������� ����������� ������ �� ������� CONV_FILES_TEMP.
                            ���������� ������ �� ������� CONV_FILES_TEMP, � ����� ��������� ����������� �� ����� CONV_RD_TEMP');
      end if;
      select STARTINDEX into CONVFILESID from CONV_ID_TEMP where TABLENAME = 'CONVFILE';
      --�������� ��������� ID ��� ���������� �� �� ��������
      select CONVRDSEQ.NEXTVAL - 1 into CONVRDID from DUAL;
      --��������� ID ��������� ��
      merge into CONV_ID_TEMP CONV_ID using DUAL on (CONV_ID.TABLENAME = TABLE_NAME)
      when not matched then
      insert (TABLENAME, STARTINDEX) values (TABLE_NAME, CONVRDID)
      when matched then
      update set STARTINDEX = CONVRDID;
      P$DISABLECONSTRAINT('CONV_RD'); -- ���������� ����������� � ��������
      execute immediate 'INSERT /*+ APPEND*/ INTO CONV_RD (ID,FILEID,IDENTITY,TYPE,LOADDATE,NOCODE,OGRN,FULLNAME) select /*+ PARALLEL(8)*/ ID+' || CONVRDID || ',FILEID+' || CONVFILESID || ',IDENTITY,TYPE,LOADDATE,NOCODE,OGRN,FULLNAME from CONV_RD_TEMP';
      SYS.DBMS_OUTPUT.PUT_LINE('��������� ����� � ������� CONV_RD=' || sql%rowcount);
      commit;
      select max(ID) into CONVRDID from CONV_RD;
      P$CORRECT_SEQUENCE('CONVRDSEQ', CONVRDID);
      P$ENABLECONSTRAINT('CONV_RD'); -- ��������� ����������� � ��������
      merge into CONV_ID_TEMP CONV_ID using DUAL on (CONV_ID.TABLENAME = TABLE_NAME)
      when not matched then
      insert (TABLENAME, ENDINDEX) values (TABLE_NAME, CONVRDID)
      when matched then
      update set ENDINDEX = CONVRDID;
    elsif TABLE_NAME = 'CONVRK' then --������������ �������� � ����������� �K
      --    SYS.DBMS_OUTPUT.PUT_LINE('files: Part_Name ="'||Part_Name||'"');
      --����������� ��������� ������ ���������� �� �� ������� CONV_ID_TEMP
      select count(1) into COUNT_CHECK from CONV_ID_TEMP where TABLENAME = 'CONVRD';
      if COUNT_CHECK = 0 then RAISE_APPLICATION_ERROR(-20100, '�� ����������� ����������� ������ �� ������� CONV_RD_TEMP.
                            ���������� ������ �� ������� CONV_RD_TEMP, � ����� ��������� ����������� �� ����� CONV_RK_TEMP');
      end if;
      select STARTINDEX into CONVRDID from CONV_ID_TEMP where TABLENAME = 'CONVRD';
      --�������� ��������� ID ��� ���������� �� �� ��������
      select CONVRKSEQ.NEXTVAL - 1 into CONVRKID from DUAL;
      --��������� ID �����
      merge into CONV_ID_TEMP CONV_ID using DUAL on (CONV_ID.TABLENAME = TABLE_NAME)
      when not matched then
      insert (TABLENAME, STARTINDEX) values (TABLE_NAME, CONVRKID)
      when matched then
      update set STARTINDEX = CONVRKID;
      P$DISABLECONSTRAINT('CONV_RK'); -- ���������� ����������� � ��������
      --������� ������
      execute immediate 'CREATE UNIQUE INDEX INDEX1 ON CONV_RK_TEMP (ID ASC)';
      select ceil(MAX(ID) / STEP_NUM) into MAX_ID from CONV_RK_TEMP;
      select MIN(ID) into MIN_ID from CONV_RK_TEMP;
      for I in MIN_ID..MAX_ID
      loop
        select (I - 1) * STEP_NUM + 1 into PREV_ID from DUAL;
        execute immediate 'INSERT /*+ APPEND*/ INTO CONV_RK (ID, RDID, IDENTITY, UNID, STATEREGNUM) select ID+' || CONVRKID || ',RDID+' || CONVRDID || ',IDENTITY, UNID, STATEREGNUM from CONV_RK_TEMP where id between ' || PREV_ID || ' and ' || I * STEP_NUM;
        DBMS_OUTPUT.PUT_LINE('FROM ' || PREV_ID || ' TO ' || I * STEP_NUM);
      end loop;
      DBMS_OUTPUT.PUT_LINE('max_id = ' || MAX_ID);
      DBMS_OUTPUT.PUT_LINE('min_id = ' || MIN_ID);
      execute immediate 'drop index "FNS_UD_EA_COMMON_USER"."INDEX1"';
      --    execute IMMEDIATE 'INSERT /*+ APPEND*/ INTO CONV_RK (ID, RDID, IDENTITY, UNID, STATEREGNUM) select /*+ PARALLEL(8)*/ ID+'||CONVRKId||',RDID+'||CONVRDId||',IDENTITY, UNID, STATEREGNUM from CONV_RK_TEMP';
      --    SYS.DBMS_OUTPUT.PUT_LINE('��������� ����� � ������� CONV_RK='||sql%rowcount);
      --    commit;
      --��������� ������� ������
      select max(ID) into CONVRKID from CONV_RK;
      P$CORRECT_SEQUENCE('CONVRKSEQ', CONVRKID);
      P$ENABLECONSTRAINT('CONV_RK'); -- ��������� ����������� � ��������
      merge into CONV_ID_TEMP CONV_ID using DUAL on (CONV_ID.TABLENAME = TABLE_NAME)
      when not matched then
      insert (TABLENAME, ENDINDEX) values (TABLE_NAME, CONVRKID)
      when matched then
      update set ENDINDEX = CONVRKID;
    elsif TABLE_NAME = 'CONVRDOC' then --������������ �������� � ����������� ����
      --    SYS.DBMS_OUTPUT.PUT_LINE('files: Part_Name ="'||Part_Name||'"');
      --����������� ��������� ������ ���������� �� �� ������� CONV_ID_TEMP
      select count(1) into COUNT_CHECK from CONV_ID_TEMP where TABLENAME = 'CONVRK';
      if COUNT_CHECK = 0 then RAISE_APPLICATION_ERROR(-20100, '�� ����������� ����������� ������ �� ������� CONV_RK_TEMP.
                            ���������� ������ �� ������� CONV_RK_TEMP, � ����� ��������� ����������� �� ����� CONV_RDOC_TEMP');
      end if;
      select STARTINDEX into CONVRKID from CONV_ID_TEMP where TABLENAME = 'CONVRK';
      --�������� ��������� ID ��� ���������� �� �� ��������
      select CONVRDOCSEQ.NEXTVAL - 1 into CONVRDOCID from DUAL;
      --��������� ID �����
      merge into CONV_ID_TEMP CONV_ID using DUAL on (CONV_ID.TABLENAME = TABLE_NAME)
      when not matched then
      insert (TABLENAME, STARTINDEX) values (TABLE_NAME, CONVRDOCID)
      when matched then
      update set STARTINDEX = CONVRDOCID;
      P$DISABLECONSTRAINT('CONV_Rdoc'); -- ���������� ����������� � ��������
      --������� ������
      execute immediate 'CREATE UNIQUE INDEX INDEX1 ON CONV_RDOC_TEMP (ID ASC)';
      select ceil(MAX(ID) / STEP_NUM) into MAX_ID from CONV_RDOC_TEMP;
      select MIN(ID) into MIN_ID from CONV_RDOC_TEMP;
      for I in MIN_ID..MAX_ID
      loop
        select (I - 1) * STEP_NUM + 1 into PREV_ID from DUAL;
        execute immediate 'INSERT /*+ APPEND*/ INTO CONV_RDOC (ID, RKID, IDENTITY, NOMDOC) select ID+' || CONVRDOCID || ',RKID+' || CONVRKID || ',IDENTITY, NOMDOC from CONV_RDOC_TEMP where id between ' || PREV_ID || ' and ' || I * STEP_NUM;
        DBMS_OUTPUT.PUT_LINE('FROM ' || PREV_ID || ' TO ' || I * STEP_NUM);
      end loop;
      DBMS_OUTPUT.PUT_LINE('max_id = ' || MAX_ID);
      DBMS_OUTPUT.PUT_LINE('min_id = ' || MIN_ID);
      execute immediate 'drop index "FNS_UD_EA_COMMON_USER"."INDEX1"';
      --    execute IMMEDIATE 'INSERT /*+ APPEND*/ INTO CONV_RDOC (ID, RKID, IDENTITY, NOMDOC) select ID+'||CONVRDOCId||',RKID+'||CONVRKId||',IDENTITY, NOMDOC from CONV_RDOC_TEMP';
      --    SYS.DBMS_OUTPUT.PUT_LINE('��������� ����� � ������� CONV_RDOC='||sql%rowcount);
      --    commit;
      --��������� ������� ������
      select max(ID) into CONVRDOCID from CONV_RDOC;
      P$CORRECT_SEQUENCE('CONVRDOCSEQ', CONVRDOCID);
      P$ENABLECONSTRAINT('CONV_Rdoc'); -- ��������� ����������� � ��������
      merge into CONV_ID_TEMP CONV_ID using DUAL on (CONV_ID.TABLENAME = TABLE_NAME)
      when not matched then
      insert (TABLENAME, ENDINDEX) values (TABLE_NAME, CONVRDOCID)
      when matched then
      update set ENDINDEX = CONVRDOCID;
    else
      RAISE_APPLICATION_ERROR(-20200, '���������� ��� ������. Part_Name ="' || TABLE_NAME || '".
                            ���������� �������� "CONVFILE","CONVRD","CONVRK","CONVRDoc"');
    end if;
    --  SELECT CONVFILESSEQ.Nextval-1 INTO CONVFILESId FROM dual;
    --  SELECT CONVRDSEQ.Nextval-1 INTO CONVRDId FROM dual;
    --  SELECT CONVRKSEQ.Nextval-1 INTO CONVRKId FROM dual;
    --  SELECT CONVRDOCSEQ.Nextval-1 INTO CONVRDOCId FROM dual;
    --  SYS.DBMS_OUTPUT.PUT_LINE('CONVRDId='||CONVRDId||', CONVRKId='||CONVRKId||', CONVRDOCId='||CONVRDOCId||', CONVFILESId='||CONVFILESId);
    --�������� � ������� CONV_FILES
    --  execute IMMEDIATE 'INSERT INTO CONV_FILES (ID, RUNID, FILEPATH) SELECT ID+'||CONVFILESId||',RUNID, FILEPATH FROM CONV_FILES_TEMP';
    --    SYS.DBMS_OUTPUT.PUT_LINE('��������� ����� � ������� CONV_FILES='||sql%rowcount);
    --�������� � ������� CONV_RD
    --   execute IMMEDIATE 'INSERT INTO CONV_RD (ID,FILEID,IDENTITY,TYPE,LOADDATE,NOCODE,OGRN,FULLNAME) select ID+'||CONVRDId||',FILEID+'||CONVFILESId||',IDENTITY,TYPE,LOADDATE,NOCODE,OGRN,FULLNAME from CONV_RD_TEMP';
    --  SYS.DBMS_OUTPUT.PUT_LINE('��������� ����� � ������� CONV_RD='||sql%rowcount);
    --�������� � ������� CONV_RK
    --    execute IMMEDIATE 'INSERT INTO CONV_RK (ID, RDID, IDENTITY, UNID, STATEREGNUM) select ID+'||CONVRKId||',RDID+'||CONVRDId||',IDENTITY, UNID, STATEREGNUM from CONV_RK_TEMP';
    --  SYS.DBMS_OUTPUT.PUT_LINE('��������� ����� � ������� CONV_RK='||sql%rowcount);
    --�������� � ������� CONV_RDOC
    --  execute IMMEDIATE 'INSERT INTO CONV_RDOC (ID, RKID, IDENTITY, NOMDOC) select ID+'||CONVRDOCId||',RKID+'||CONVRKId||',IDENTITY, NOMDOC from CONV_RDOC_TEMP';
    --  SYS.DBMS_OUTPUT.PUT_LINE('��������� ����� � ������� CONV_RDOC='||sql%rowcount);
    --  select max(id) into CONVRDId from CONV_RD;
    --  select max(id) into CONVRKId from CONV_RK;
    --  select max(id) into CONVRDOCId from CONV_RDOC;
    --  select max(id) into CONVFILESId from CONV_FILES;
    --  SYS.DBMS_OUTPUT.PUT_LINE('CONVRDId='||CONVRDId||', CONVRKId='||CONVRKId||', CONVRDOCId='||CONVRDOCId||', CONVFILESId='||CONVFILESId);
    --  P$CORRECT_SEQUENCE( 'CONVRDSEQ', CONVRDId );
    --  P$CORRECT_SEQUENCE( 'CONVRKSEQ', CONVRKId );
    --  P$CORRECT_SEQUENCE( 'CONVRDOCSEQ', CONVRDOCId );
    --  P$CORRECT_SEQUENCE( 'CONVFILESSEQ', CONVFILESId );
  end P$COPYCONVFROMTEMP;
/