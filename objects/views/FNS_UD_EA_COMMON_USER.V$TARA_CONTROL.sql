create force view FNS_UD_EA_COMMON_USER.V$TARA_CONTROL (PD_RECORD_NUMBER, EA_RECORD_NUMBER, MODIFY_DATE, ACT_RECORD_NUMBER, TARA_INVENTORY_NUMBER, TARA_RECORD_NUMBER, DOC_BATCH_COUNT, DOC_COUNT, SHEET_COUNT, TASK_SENDING_DATE, PROTOCOL_RECEIVING_DATE, RCV_OPERATION_EXECUTING_DATE, TARA_STATUS, KNO) as
  select
    DV.U16D8_RECORDNUMBER as PD_RECORD_NUMBER, -- ===== ProcessDoc recordNumber
    DVEA.U16D8_RECORDNUMBER as EA_RECORD_NUMBER, -- ===== EA_RD  recordNumber
    DV.MODIFY_DATE as MODIFY_DATE, -- ===== ProcessDoc modify time
    DVH2.U16D8_RECORDNUMBER as ACT_RECORD_NUMBER, -- ===== FNSActHandover recordNumber
    DVH.UD1A8_TARAINVENTORYNUMBER as TARA_INVENTORY_NUMBER,
    DVH.U16D8_RECORDNUMBER as TARA_RECORD_NUMBER,
    DVHEACNT.CNT as DOC_BATCH_COUNT, -- ===== Count of documents in parent document from EA_RD
    DVH.U00B6_DOCCOUNT as DOC_COUNT, -- ===== Count of documents in UD_SuppliedSheet
    DVH.U7E76_SHEETCOUNT as SHEET_COUNT, -- ===== Count of sheets in UD_SuppliedSheet
    DVH2.U0FF3_RECORDDATE as TASK_SENDING_DATE, -- ===== Sending date of UD_ActHandover
    DVH2CH1.U0FF3_RECORDDATE as PROTOCOL_RECEIVING_DATE, -- ===== FNSActHandover
    DV.U0FF3_RECORDDATE as RCV_OPERATION_EXECUTING_DATE, -- ===== DPCReceiptTara
    NVL(TRANSL.TRANSLATED, DVH.U5B08_STATUS) as TARA_STATUS,
    DVH2.UEA08_TAXAUTHORITYCODE as KNO
  from FNS_UD_CE_USER.DOCVERSION DV
  join FNS_UD_CE_USER.CLASSDEFINITION CD0 on DV.OBJECT_CLASS_ID = CD0.OBJECT_ID and CD0.SYMBOLIC_NAME = 'UD_ProcessDoc'
  left join FNS_EA_CE_USER.DOCVERSION DVEA on DV.U24A8_EANUMBER = DVEA.U16D8_RECORDNUMBER
  left join FNS_EA_CE_USER.LINK EALNK on EALNK.TAIL_ID = DVEA.OBJECT_ID
  left join (
              select
                count(*) as CNT,
                EALNK.HEAD_ID as HEAD_ID
              from FNS_EA_CE_USER.LINK EALNK
              join FNS_EA_CE_USER.DOCVERSION DVEA on DVEA.OBJECT_ID = EALNK.TAIL_ID
              join FNS_EA_CE_USER.CLASSDEFINITION CD on DVEA.OBJECT_CLASS_ID = CD.OBJECT_ID and CD.SYMBOLIC_NAME = 'EA_RD'
              group by EALNK.HEAD_ID
            ) DVHEACNT on DVHEACNT.HEAD_ID = EALNK.HEAD_ID
  left join FNS_UD_CE_USER.DOCVERSION DVH on DVH.U16D8_RECORDNUMBER = DV.U2228_PARENTRECORDNUMBER    -- ===== UD_SuppliedSheet ======
  left join FNS_UD_EA_COMMON_USER.TRANSLATIONS TRANSL on (TRANSL.TR_TYPE = 'DOC_STATUS' and TRANSL.ORIGINAL = DVH.U5B08_STATUS and TRANSL.DOC_KIND = 'UD_SUPPLIEDSHEET')
  left join FNS_UD_CE_USER.DOCVERSION DVH2 on DVH2.U16D8_RECORDNUMBER = DVH.U2228_PARENTRECORDNUMBER  -- ====== UD_ActHandover =====
  left join FNS_UD_CE_USER.LINK LNK3 on LNK3.HEAD_ID = DVH2.OBJECT_ID
  left join FNS_UD_CE_USER.DOCVERSION DVH2CH1 on DVH2CH1.OBJECT_ID = LNK3.TAIL_ID
  left join FNS_UD_CE_USER.CLASSDEFINITION CD3 on DVH2CH1.OBJECT_CLASS_ID = CD3.OBJECT_ID and CD3.SYMBOLIC_NAME = 'UD_ReceiptTaraProtocol';