create force view FNS_UD_EA_COMMON_USER.V$NSI$PRINT_CENTERS_CAPACITIES (ID, CPS, PRINT_TYPE, PRINT_TYPE_STRING, FIO, CAPACITY, CREATE_DATE, IS_ANNULLED, IS_ANNULLED_STRING) as
  select
    ID,
    RECEIVER_CODE as CPS,
    PRINTING_TYPE as PRINT_TYPE,
    /*
    case PRINTING_TYPE
      when 0 then '����������'
      when 1 then '�� ���������'
    end*/
    TRS2.TRANSLATED as PRINT_TYPE_STRING,
    case
    when FIO is null then '<�� ���������>'
    else FIO end as
    FIO,
    CAPACITY,
    CREATE_DATE,
    IS_ANNULLED,
    case IS_ANNULLED
    when 0 then '����������'
    when 1 then '�� ����������'
    end as IS_ANNULLED_STRING
  from FNS_UD_EA_COMMON_USER.NSI$PRINT_CENTERS_CAPACITIES
  inner join FNS_UD_EA_COMMON_USER.TRANSLATIONS TRS2 on (TRS2.TR_TYPE = 'PRINTING_TYPE' and TRS2.DOC_KIND = 'PO$' and TRS2.ORIGINAL = TO_CHAR(PRINTING_TYPE));