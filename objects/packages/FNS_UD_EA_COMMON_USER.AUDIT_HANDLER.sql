create package FNS_UD_EA_COMMON_USER.AUDIT_HANDLER as
  procedure HANDLE_BUFFER_ACCESS(OBJECT_SCHEMA VARCHAR2, OBJECT_NAME VARCHAR2, POLICY_NAME VARCHAR2);
end;
/
create package body FNS_UD_EA_COMMON_USER.AUDIT_HANDLER as
  procedure HANDLE_BUFFER_ACCESS(OBJECT_SCHEMA VARCHAR2, OBJECT_NAME VARCHAR2, POLICY_NAME VARCHAR2) is
  pragma autonomous_transaction;
    begin
      insert into AU_BUFFER_ACCESS (ID, TYPE, WHO_DID_IT, WHEN_WAS_IT, SQL_EXECUTED)
      values (HIBERNATE_SEQUENCE.NEXTVAL, OBJECT_NAME, user, systimestamp, sys_context('userenv', 'current_sql', 4000));
      commit;
    end;
end;
/