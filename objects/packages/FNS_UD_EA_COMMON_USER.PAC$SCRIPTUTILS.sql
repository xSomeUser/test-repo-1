create package FNS_UD_EA_COMMON_USER.PAC$SCRIPTUTILS authid current_user as
  function REGISTER_SCRIPT_EXECUTION(SCR_NUMBER NUMBER, SCR_DESC VARCHAR2, REQUIRE_ORDER BOOLEAN) return BOOLEAN;
  function CONFIRM_SCRIPT_EXECUTION(SCR_NUMBER NUMBER) return BOOLEAN;
  procedure ABORT_SCRIPT_EXECUTION(SCR_NUMBER NUMBER, ERR_MESSAGE VARCHAR2);
  procedure DROP_TABLE_IF_EXISTS(SCHEMENAME VARCHAR2, TABLENAME VARCHAR2);
  procedure DROP_INDEX_IF_EXISTS(SCHEMENAME VARCHAR2, INDEXNAME VARCHAR2);
  procedure DROP_ALL_INDEXES(SCHEMENAME VARCHAR2, TABLENAME VARCHAR2);
  procedure ALTER_TABLE(SCHEMENAME VARCHAR2, TABLENAME VARCHAR2, ALTERTXT VARCHAR2);
end PAC$SCRIPTUTILS;
/
create package body FNS_UD_EA_COMMON_USER.PAC$SCRIPTUTILS as
  function REGISTER_SCRIPT_EXECUTION(SCR_NUMBER NUMBER, SCR_DESC VARCHAR2, REQUIRE_ORDER BOOLEAN) return BOOLEAN as
    CNT INT;
    begin
      if (REQUIRE_ORDER) then
        select count(*) into CNT from FNS_UD_EA_COMMON_USER.SCRIPTREGISTRATION where SCRIPT_NUMBER = SCR_NUMBER - 1 and SUCCESS_FLAG = 'Y';
        if CNT <> 1 then
          return false;
        end if;
      end if;
      select count(*) into CNT from FNS_UD_EA_COMMON_USER.SCRIPTREGISTRATION where SCRIPT_NUMBER = SCR_NUMBER;
      if CNT = 1 then
        update FNS_UD_EA_COMMON_USER.SCRIPTREGISTRATION
        set SCRIPT_DESC = SCR_DESC,
          SUCCESS_FLAG = 'N',
          APPLY_DT = SYSTIMESTAMP,
          APPLY_USER = USER
        where SCRIPT_NUMBER = SCR_NUMBER;
      else
        insert into FNS_UD_EA_COMMON_USER.SCRIPTREGISTRATION
        (SCRIPT_NUMBER, SCRIPT_DESC)
        values (SCR_NUMBER, SCR_DESC);
      end if;
      return true;
      exception
      when others then
      return false;
    end REGISTER_SCRIPT_EXECUTION;
  function CONFIRM_SCRIPT_EXECUTION(SCR_NUMBER NUMBER) return BOOLEAN as
    begin
      update FNS_UD_EA_COMMON_USER.SCRIPTREGISTRATION
      set
        SUCCESS_FLAG = 'Y',
        ERROR_DESC = ''
      where SCRIPT_NUMBER = SCR_NUMBER;
      return true;
      exception
      when others then
      return false;
    end CONFIRM_SCRIPT_EXECUTION;
  procedure ABORT_SCRIPT_EXECUTION(SCR_NUMBER NUMBER, ERR_MESSAGE VARCHAR2) as
    begin
      update FNS_UD_EA_COMMON_USER.SCRIPTREGISTRATION
      set
        SUCCESS_FLAG = 'N',
        ERROR_DESC = ERR_MESSAGE
      where SCRIPT_NUMBER = SCR_NUMBER;
      exception
      when others then null;
    end ABORT_SCRIPT_EXECUTION;
  procedure DROP_INDEX_IF_EXISTS(SCHEMENAME VARCHAR2, INDEXNAME VARCHAR2) as
      NOT_EXISTS exception;
  pragma exception_init ( NOT_EXISTS, -1418 );
    begin
      execute immediate 'DROP INDEX ' || SCHEMENAME || '.' || INDEXNAME;
      DBMS_OUTPUT.put_line('Index ' || SCHEMENAME || '.' || INDEXNAME || ' dropped');
      exception
      when NOT_EXISTS then
      null;
    end DROP_INDEX_IF_EXISTS;
  procedure DROP_TABLE_IF_EXISTS(SCHEMENAME VARCHAR2, TABLENAME VARCHAR2) as
      NOT_EXISTS exception;
  pragma exception_init ( NOT_EXISTS, -942 );
    begin
      execute immediate 'Drop TABLE ' || SCHEMENAME || '.' || TABLENAME;
      DBMS_OUTPUT.put_line('Table ' || SCHEMENAME || '.' || TABLENAME || 'dropped');
      exception
      when NOT_EXISTS then
      null;
    end DROP_TABLE_IF_EXISTS;
  procedure DROP_ALL_INDEXES(SCHEMENAME VARCHAR2, TABLENAME VARCHAR2) as
    SQLTXT VARCHAR2(1000);
    begin
      for I in (select *
      from ALL_INDEXES
      where TABLE_OWNER = SCHEMENAME and TABLE_NAME like TABLENAME and INDEX_NAME not like '%_PK')
      loop
        SQLTXT := 'DROP INDEX ' || I.OWNER || '.' || I.INDEX_NAME;
        execute immediate SQLTXT;
        DBMS_OUTPUT.put_line('Index ' || I.OWNER || '.' || I.INDEX_NAME || ' dropped');
      end loop;
      exception
      when others then
      null;
    end DROP_ALL_INDEXES;
  procedure ALTER_TABLE(SCHEMENAME VARCHAR2, TABLENAME VARCHAR2, ALTERTXT VARCHAR2) as
    SQLTXT VARCHAR2(1000);
    begin
      SQLTXT := 'ALTER TABLE ' || SCHEMENAME || '.' || TABLENAME || ' ' || ALTERTXT;
      execute immediate SQLTXT;
      /*
     exception
       when OTHERS then
           null;
       */
    end ALTER_TABLE;
end PAC$SCRIPTUTILS;
/