create package FNS_UD_EA_COMMON_USER.PAC$PATH_UPDATER as
  ROWS_BATCH_SIZE NUMBER(20, 0) := 10000;
  function GET_FILENAME_VARCHAR2_VALUE(P_ID FTEWEB.FILE_SPACE_ENTRY.ID%TYPE) return VARCHAR2;
  procedure LOG(MESSAGE VARCHAR2);
  procedure PR_FILEPATH(OLD_PATH VARCHAR2, NEW_PATH VARCHAR2);
  procedure BR_URLPATH(OLD_PATH VARCHAR2, NEW_PATH VARCHAR2);
  procedure FS_LOCATION(OLD_PATH VARCHAR2, NEW_PATH VARCHAR2);
  procedure WGC_WGNAME(OLD_PATH VARCHAR2, NEW_PATH VARCHAR2);
  procedure FSE_FILENAME(OLD_PATH VARCHAR2, NEW_PATH VARCHAR2);
  procedure IR_FILE_PATH(OLD_PATH VARCHAR2, NEW_PATH VARCHAR2);
  procedure UDDV_URLPATH(OLD_PATH VARCHAR2, NEW_PATH VARCHAR2);
  procedure EADV_URLPATH(OLD_PATH VARCHAR2, NEW_PATH VARCHAR2);
  procedure DO_ALL(OLD_PATH VARCHAR2, NEW_PATH VARCHAR2);
end PAC$PATH_UPDATER;
/
create package body FNS_UD_EA_COMMON_USER.PAC$PATH_UPDATER as
  function GET_FILENAME_VARCHAR2_VALUE(P_ID FTEWEB.FILE_SPACE_ENTRY.ID%TYPE) return VARCHAR2 as
    V_LONG LONG;
    begin
      select FILENAME into V_LONG
      from FTEWEB.FILE_SPACE_ENTRY
      where ID = P_ID;
      return substr(V_LONG, 1, 4000);
    end;
  procedure LOG(MESSAGE VARCHAR2) as
    begin
      insert into FNS_UD_EA_COMMON_USER.PATH_UPDATER_LOG (TIME, MESSAGE) values (SYSTIMESTAMP, MESSAGE);
      commit;
    end;
  procedure PR_FILEPATH(OLD_PATH VARCHAR2, NEW_PATH VARCHAR2) as
    cursor CUR is
      select ID
      from FNS_UD_EA_COMMON_USER.PUBLISHED_RESOURCE
      where FILE_PATH like OLD_PATH || '%';
    type T_ID is TABLE OF FNS_UD_EA_COMMON_USER.PUBLISHED_RESOURCE.ID%TYPE index by pls_integer;
    V_ID T_ID;
    begin
      LOG('PR_FILEPATH begin');
      open CUR;
      loop
        fetch CUR bulk collect into V_ID limit ROWS_BATCH_SIZE;
        forall IDX in 1 .. V_ID.COUNT
        update FNS_UD_EA_COMMON_USER.PUBLISHED_RESOURCE
        set FILE_PATH = REPLACE(FILE_PATH, OLD_PATH, NEW_PATH)
        where ID in (V_ID(IDX));
        commit;
        LOG(V_ID.COUNT || ' rows updated');
        exit when CUR%notfound;
      end loop;
      close CUR;
      LOG('PR_FILEPATH end');
    end;
  procedure BR_URLPATH(OLD_PATH VARCHAR2, NEW_PATH VARCHAR2) as
    cursor CUR is
      select ID
      from FNS_UD_EA_COMMON_USER.BUFFER_RESOURCE
      where FILE_PATH like OLD_PATH || '%';
    type T_ID is TABLE OF FNS_UD_EA_COMMON_USER.BUFFER_RESOURCE.ID%TYPE index by pls_integer;
    V_ID T_ID;
    begin
      LOG('BR_URLPATH begin');
      open CUR;
      loop
        fetch CUR bulk collect into V_ID limit ROWS_BATCH_SIZE;
        forall IDX in 1 .. V_ID.COUNT
        update FNS_UD_EA_COMMON_USER.BUFFER_RESOURCE
        set FILE_PATH = REPLACE(FILE_PATH, OLD_PATH, NEW_PATH)
        where ID in (V_ID(IDX));
        commit;
        LOG(V_ID.COUNT || ' rows updated');
        exit when CUR%notfound;
      end loop;
      close CUR;
      LOG('BR_URLPATH end');
    end;
  procedure FS_LOCATION(OLD_PATH VARCHAR2, NEW_PATH VARCHAR2) as
    V_OLD_PATH VARCHAR2(4000) := replace(OLD_PATH, '\', '/');
    V_NEW_PATH VARCHAR2(4000) := replace(NEW_PATH, '\', '/');
    cursor CUR is
      select NAME
      from FTEWEB.FILE_SPACE
      where LOCATION like V_OLD_PATH || '%';
    type T_ID is TABLE OF FTEWEB.FILE_SPACE.NAME%TYPE index by pls_integer;
    V_ID T_ID;
    begin
      LOG('FS_LOCATION begin');
      open CUR;
      loop
        fetch CUR bulk collect into V_ID limit ROWS_BATCH_SIZE;
        forall IDX in 1 .. V_ID.COUNT
        update FTEWEB.FILE_SPACE
        set LOCATION = REPLACE(LOCATION, V_OLD_PATH, V_NEW_PATH)
        where NAME in (V_ID(IDX));
        commit;
        LOG(V_ID.COUNT || ' rows updated');
        exit when CUR%notfound;
      end loop;
      close CUR;
      LOG('FS_LOCATION end');
    end;
  procedure WGC_WGNAME(OLD_PATH VARCHAR2, NEW_PATH VARCHAR2) as
    V_OLD_PATH VARCHAR2(4000) := replace(OLD_PATH, '\', '/');
    V_NEW_PATH VARCHAR2(4000) := replace(NEW_PATH, '\', '/');
    cursor CUR is
      select WEBGATEWAY_NAME
      from FTEWEB.WEBGATEWAY_CONFIG
      where FILESPACE_ROOT_LOCATION like V_OLD_PATH || '%';
    type T_ID is TABLE OF FTEWEB.WEBGATEWAY_CONFIG.WEBGATEWAY_NAME%TYPE index by pls_integer;
    V_ID T_ID;
    begin
      LOG('WGC_WGNAME begin');
      open CUR;
      loop
        fetch CUR bulk collect into V_ID limit ROWS_BATCH_SIZE;
        forall IDX in 1 .. V_ID.COUNT
        update FTEWEB.WEBGATEWAY_CONFIG
        set FILESPACE_ROOT_LOCATION = REPLACE(FILESPACE_ROOT_LOCATION, V_OLD_PATH, V_NEW_PATH)
        where WEBGATEWAY_NAME in (V_ID(IDX));
        commit;
        LOG(V_ID.COUNT || ' rows updated');
        exit when CUR%notfound;
      end loop;
      close CUR;
      LOG('WGC_WGNAME end');
    end;
  procedure FSE_FILENAME(OLD_PATH VARCHAR2, NEW_PATH VARCHAR2) as
    V_OLD_PATH VARCHAR2(4000) := replace(OLD_PATH, '\', '/');
    V_NEW_PATH VARCHAR2(4000) := replace(NEW_PATH, '\', '/');
    cursor CUR is
      select ID
      from FTEWEB.FILE_SPACE_ENTRY
      where GET_FILENAME_VARCHAR2_VALUE(ID) like V_OLD_PATH || '%';
    type T_ID is TABLE OF FTEWEB.FILE_SPACE_ENTRY.ID%TYPE index by pls_integer;
    V_ID T_ID;
    V_FILENAME VARCHAR2(4000);
    begin
      LOG('FSE_FILENAME begin');
      open CUR;
      loop
        fetch CUR bulk collect into V_ID limit ROWS_BATCH_SIZE;
        for IDX in 1 .. V_ID.COUNT loop
          select REPLACE(GET_FILENAME_VARCHAR2_VALUE(ID), V_OLD_PATH, V_NEW_PATH) into V_FILENAME from FTEWEB.FILE_SPACE_ENTRY
          where ID = V_ID(IDX);
          update FTEWEB.FILE_SPACE_ENTRY set FILENAME = V_FILENAME where ID = V_ID(IDX);
        end loop;
        commit;
        LOG(V_ID.COUNT || ' rows updated');
        exit when CUR%notfound;
      end loop;
      close CUR;
      LOG('FSE_FILENAME end');
    end;
  procedure IR_FILE_PATH(OLD_PATH VARCHAR2, NEW_PATH VARCHAR2) as
    cursor CUR is
      select ID
      from FNS_UD_EA_COMMON_USER.PO$INFORMATION_RESOURCE
      where FILE_PATH like OLD_PATH || '%';
    type T_ID is TABLE OF FNS_UD_EA_COMMON_USER.PO$INFORMATION_RESOURCE.ID%TYPE index by pls_integer;
    V_ID T_ID;
    begin
      LOG('IR_FILE_PATH begin');
      open CUR;
      loop
        fetch CUR bulk collect into V_ID limit ROWS_BATCH_SIZE;
        forall IDX in 1 .. V_ID.COUNT
        update FNS_UD_EA_COMMON_USER.PO$INFORMATION_RESOURCE
        set FILE_PATH = REPLACE(FILE_PATH, OLD_PATH, NEW_PATH)
        where ID in (V_ID(IDX));
        commit;
        LOG(V_ID.COUNT || ' rows updated');
        exit when CUR%notfound;
      end loop;
      close CUR;
      LOG('IR_FILE_PATH end');
    end;
  procedure UDDV_URLPATH(OLD_PATH VARCHAR2, NEW_PATH VARCHAR2) as
    cursor CUR is
      select OBJECT_ID
      from FNS_UD_CE_USER.DOCVERSION
      where U8518_URLPATH like OLD_PATH || '%'
      order by U0FF3_RECORDDATE desc;
    type T_OBJ_ID is TABLE OF FNS_UD_CE_USER.DOCVERSION.OBJECT_ID%TYPE index by pls_integer;
    V_OBJ_ID T_OBJ_ID;
    begin
      LOG('UDDV_URLPATH begin');
      open CUR;
      loop
        fetch CUR bulk collect into V_OBJ_ID limit ROWS_BATCH_SIZE;
        forall IDX in 1 .. V_OBJ_ID.COUNT
        update FNS_UD_CE_USER.DOCVERSION set U8518_URLPATH = REPLACE(U8518_URLPATH, OLD_PATH, NEW_PATH)
        where OBJECT_ID in (V_OBJ_ID(IDX));
        commit;
        LOG(V_OBJ_ID.COUNT || ' rows updated');
        exit when CUR%notfound;
      end loop;
      close CUR;
      LOG('UDDV_URLPATH end');
    end;
  procedure EADV_URLPATH(OLD_PATH VARCHAR2, NEW_PATH VARCHAR2) as
    cursor CUR is
      select OBJECT_ID
      from FNS_EA_CE_USER.DOCVERSION
      where U8518_URLPATH like OLD_PATH || '%'
      order by U0FF3_RECORDDATE desc;
    type T_OBJ_ID is TABLE OF FNS_EA_CE_USER.DOCVERSION.OBJECT_ID%TYPE index by pls_integer;
    V_OBJ_ID T_OBJ_ID;
    begin
      LOG('EADV_URLPATH begin');
      open CUR;
      loop
        fetch CUR bulk collect into V_OBJ_ID limit ROWS_BATCH_SIZE;
        forall IDX in 1 .. V_OBJ_ID.COUNT
        update FNS_EA_CE_USER.DOCVERSION set U8518_URLPATH = REPLACE(U8518_URLPATH, OLD_PATH, NEW_PATH)
        where OBJECT_ID in (V_OBJ_ID(IDX));
        commit;
        LOG(V_OBJ_ID.COUNT || ' rows updated');
        exit when CUR%notfound;
      end loop;
      close CUR;
      LOG('EADV_URLPATH end');
    end;
  procedure DO_ALL(OLD_PATH VARCHAR2, NEW_PATH VARCHAR2) as
    begin
      LOG('DO_ALL begin');
      PR_FILEPATH(OLD_PATH, NEW_PATH);
      BR_URLPATH(OLD_PATH, NEW_PATH);
      FS_LOCATION(OLD_PATH, NEW_PATH);
      WGC_WGNAME(OLD_PATH, NEW_PATH);
      FSE_FILENAME(OLD_PATH, NEW_PATH);
      IR_FILE_PATH(OLD_PATH, NEW_PATH);
      UDDV_URLPATH(OLD_PATH, NEW_PATH);
      EADV_URLPATH(OLD_PATH, NEW_PATH);
      LOG('DO_ALL end');
    end;
end PAC$PATH_UPDATER;
/