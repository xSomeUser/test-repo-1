begin
  DBMS_SCHEDULER.create_job('"UD_EA_REGULAR_TABLES_CLEANER"',
                            JOB_TYPE=>'STORED_PROCEDURE', JOB_ACTION=>
                            'FNS_UD_EA_COMMON_USER.CLEAN_TABLES'
  , NUMBER_OF_ARGUMENTS=>0,
                            START_DATE=>TO_TIMESTAMP_TZ('30-JUN-2015 05.10.02.025000000 PM EUROPE/MOSCOW', 'DD-MON-RRRR HH.MI.SSXFF AM TZR', 'NLS_DATE_LANGUAGE=english'), REPEAT_INTERVAL=>
                            'FREQ=DAILY;BYDAY=MON,TUE,WED,THU,FRI,SAT,SUN;BYHOUR=3;BYMINUTE=0;BYSECOND=0'
  , END_DATE=>null,
                            JOB_CLASS=>'"N3_SVC_BACKGROUND"', ENABLED=>false, AUTO_DROP=>false, COMMENTS=>
                            null
  );
  DBMS_SCHEDULER.enable('"UD_EA_REGULAR_TABLES_CLEANER"');
  commit;
end;
/