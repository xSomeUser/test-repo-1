begin
  DBMS_SCHEDULER.create_job('"J$UD_EA_IRPO_RESEND"',
                            JOB_TYPE=>'STORED_PROCEDURE', JOB_ACTION=>
                            'FNS_UD_EA_COMMON_USER.P$PO_IR_RESEND'
  , NUMBER_OF_ARGUMENTS=>0,
                            START_DATE=>TO_TIMESTAMP_TZ('29-JUL-2016 01.45.04.859000000 PM EUROPE/MOSCOW', 'DD-MON-RRRR HH.MI.SSXFF AM TZR', 'NLS_DATE_LANGUAGE=english'), REPEAT_INTERVAL=>
                            'FREQ=HOURLY;INTERVAL=4'
  , END_DATE=>null,
                            JOB_CLASS=>'"DEFAULT_JOB_CLASS"', ENABLED=>false, AUTO_DROP=>false, COMMENTS=>
                            null
  );
  DBMS_SCHEDULER.enable('"J$UD_EA_IRPO_RESEND"');
  commit;
end;
/