begin
  DBMS_SCHEDULER.create_job('"PATH_UPDATER_JOB"',
                            JOB_TYPE=>'STORED_PROCEDURE', JOB_ACTION=>
                            'FNS_UD_EA_COMMON_USER.PAC$PATH_UPDATER.DO_ALL'
  , NUMBER_OF_ARGUMENTS=>2,
                            START_DATE=>null, REPEAT_INTERVAL=>
                            null
  , END_DATE=>null,
                            JOB_CLASS=>'"DEFAULT_JOB_CLASS"', ENABLED=>false, AUTO_DROP=>false, COMMENTS=>
                            null
  );
  commit;
end;
/ 