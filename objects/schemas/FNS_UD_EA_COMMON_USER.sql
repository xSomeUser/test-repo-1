create user FNS_UD_EA_COMMON_USER identified by values 'S:4AC24E58DB1D616AF330123EB9201BC0B482347A92E46A3224EDE5213648;49020D884B321E69' default tablespace users temporary tablespace TEMP;
grant CONNECT to FNS_UD_EA_COMMON_USER;
grant RESOURCE to FNS_UD_EA_COMMON_USER;
alter user FNS_UD_EA_COMMON_USER default role CONNECT, RESOURCE;
grant unlimited tablespace to FNS_UD_EA_COMMON_USER;
grant create view to FNS_UD_EA_COMMON_USER;
grant create job to FNS_UD_EA_COMMON_USER;